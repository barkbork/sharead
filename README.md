# Sharead

*Sharead* is a very original Reddit-like with some awesome features like browsing the website without network errors!  
You can access the website on http://sharead.rousseur.fr:5555/
PHPMyAdmin is available on http://sharead.rousseur.fr:5550/ with user `cesi` and password `aled`
A dump of the database, with data, is available in this project under `db_with_data.sql`.

## Usage

### Run with Docker (recommended)

To run the project, you **must** have installed `docker`, `docker-compose` and `make`.  
Then, just head to the project folder in your favorite terminal and launch *Sharead* :

```
make start
```

After a short while, *Sharead* is available on `localhost:8888`.

### Run without Docker

You can put the project inside a server running a PHP version compatible with CodeIgniter 3, but you must reconfigure the database connection inside the file `application/config/database.php` to make it work with your database provider.

## Development tools

All of the development tools makes use of Docker, thus we strongly recommand to [install it](https://docs.docker.com/install/).

A Makefile is provided to help with the development process :
 - `make start` to run the project (the default ports are `8888` for the web server and `8080` for PhpMyAdmin)
 - `make init` to create the tables while the project is running
 - `make clean` to clean the project (removes containers) (! **wipes the entire database** !)
 - `make stop` to stop the project
 - `make restart` to restart the project
