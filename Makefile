start:
	$(info Starting Sharead with docker-compose)
	@docker-compose up -d

stop:
	$(info Stopping Sharead with docker-compose)
	@docker-compose stop

build:
	$(info Building Sharead with docker-compose)
	@docker-compose pull
	@docker-compose build --no-cache

restart:
	@make -s stop
	@make -s start

clean:
	$(info Cleaning all volumes)
	@docker-compose down
	@docker system prune --volumes --force

init:
	$(info Fix /uploads permissions)
	@chmod 777 uploads
	$(info Initializing database)
	@docker container exec -i sharead_mysql mysql -u cesi --password=aled sharead < init_db.sql
