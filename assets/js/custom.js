$(".downvote-post").click(function() {
    $.ajax({
        data: { id: $("#post-id").html() },
        url: "/index.php/posts/downvote",
        type: "POST",
        success: function(data) {
            var count_downvotes = parseInt(
                $("#downVote")
                    .find("strong")
                    .html(),
                10
            );

            if (data === "INSERT") {
                $("#downVote")
                    .find("strong")
                    .html(count_downvotes + 1);
            } else if (data === "UPDATE") {
                var count_upvotes = parseInt(
                    $("#upVote")
                        .find("strong")
                        .html(),
                    10
                );
                $("#upVote")
                    .find("strong")
                    .html(count_upvotes - 1);
                $("#downVote")
                    .find("strong")
                    .html(count_downvotes + 1);
            } else if (data === "DELETE") {
                $("#downVote")
                    .find("strong")
                    .html(count_downvotes - 1);
            }
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(".upvote-post").click(function() {
    $.ajax({
        data: { id: $("#post-id").html() },
        url: "/index.php/posts/upvote",
        type: "POST",
        success: function(data) {
            var count_upvotes = parseInt(
                $("#upVote")
                    .find("strong")
                    .html(),
                10
            );
            if (data === "INSERT") {
                $("#upVote")
                    .find("strong")
                    .html(count_upvotes + 1);
            } else if (data === "UPDATE") {
                var count_downvotes = parseInt(
                    $("#downVote")
                        .find("strong")
                        .html(),
                    10
                );
                $("#downVote")
                    .find("strong")
                    .html(count_downvotes - 1);
                $("#upVote")
                    .find("strong")
                    .html(count_upvotes + 1);
            } else if (data === "DELETE") {
                $("#upVote")
                    .find("strong")
                    .html(count_upvotes - 1);
            }
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(".downvote-comment").click(function() {
    var caller = this;
    var id = $(this)
        .attr("id")
        .replace("comment-", "");
    $.ajax({
        data: { id: id },
        url: "/index.php/comments/downvote",
        type: "POST",
        success: function(data) {
            var count_downvotes = parseInt(
                $(caller)
                    .find("strong")
                    .html(),
                10
            );
            if (data === "INSERT") {
                $(caller)
                    .find("strong")
                    .html(count_downvotes + 1);
            } else if (data === "UPDATE") {
                var count_upvotes = parseInt(
                    $(".upvote-comment#comment-" + id)
                        .find("strong")
                        .html(),
                    10
                );
                $(".upvote-comment#comment-" + id)
                    .find("strong")
                    .html(count_upvotes - 1);
                $(caller)
                    .find("strong")
                    .html(count_downvotes + 1);
            } else if (data === "DELETE") {
                $(caller)
                    .find("strong")
                    .html(count_downvotes - 1);
            }
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(".upvote-comment").click(function() {
    var caller = this;
    var id = $(this)
        .attr("id")
        .replace("comment-", "");
    $.ajax({
        data: { id: id },
        url: "/index.php/comments/upvote",
        type: "POST",
        success: function(data) {
            var count_upvotes = parseInt(
                $(caller)
                    .find("strong")
                    .html(),
                10
            );
            if (data === "INSERT") {
                $(caller)
                    .find("strong")
                    .html(count_upvotes + 1);
            } else if (data === "UPDATE") {
                var count_downvotes = parseInt(
                    $(".downvote-comment#comment-" + id)
                        .find("strong")
                        .html(),
                    10
                );
                $(".downvote-comment#comment-" + id)
                    .find("strong")
                    .html(count_downvotes - 1);
                $(caller)
                    .find("strong")
                    .html(count_upvotes + 1);
            } else if (data === "DELETE") {
                $(caller)
                    .find("strong")
                    .html(count_upvotes - 1);
            }
        },
        error: function(data) {
            console.log(data);
        }
    });
});

$(document).ready(function () {
   $(".remove-user-role").click(function () {
       var channel = $(this).data("channel");
       var user = $(this).data("user");
       var role = $(this).data("role");

       $.post("../remove_role/" + channel, {
           "channel": channel,
           "user": user,
           "role": role
       }).done(function () {
            $("#user-" + user).remove();
            var badge = $("#role-" + role + "-badge");
            badge.text(badge.text() - 1);
       }).fail(function(err) {
           console.log(err);
           alert('An unknown error occured');
       });
   });
});
