-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : db:3306
-- Généré le : lun. 10 fév. 2020 à 18:21
-- Version du serveur :  8.0.19
-- Version de PHP : 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sharead`
--

-- --------------------------------------------------------

--
-- Structure de la table `channels`
--

CREATE TABLE `channels` (
  `id` int NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `banner` varchar(2048) NOT NULL,
  `author` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `channels`
--

INSERT INTO `channels` (`id`, `name`, `description`, `banner`, `author`) VALUES
(1, 'Science', 'This community is a place to share and discuss new scientific research. Read about the latest advances in astronomy, biology, medicine, physics, social science, and more. Find and submit new publications and popular science coverage of current research.', 'http://sharead.rousseur.fr:5555/uploads/6b44a166d441b9f540e9985b58902328.jpg', 1),
(2, 'lookatmydog', 'A community founded on a simple premise - sharing pictures of our canines!', 'http://sharead.rousseur.fr:5555/uploads/3b6752c9d74a541a2e2e26a7aaa5e0ce.gif', 2),
(3, 'Formula1', 'The best independent Formula 1 community anywhere. News, stories and discussion from and about the world of Formula 1.', 'http://sharead.rousseur.fr:5555/uploads/6e099a78b7d1b216939df950f4699dab.jpg', 1),
(4, 'LordOfTheRings', 'For all things Tolkien, Lord of The Rings, and The Hobbit ...', 'http://sharead.rousseur.fr:5555/uploads/797cbce4dcd28e3e331285f41a9c141f.png', 4),
(5, 'funny', 'Welcome to Funny: Sharead\'s largest humour depository.', 'http://sharead.rousseur.fr:5555/uploads/de4ff2d47b9d3e91d26d02c85f657125.jpg', 2);

-- --------------------------------------------------------

--
-- Structure de la table `channels_roles`
--

CREATE TABLE `channels_roles` (
  `channel` int NOT NULL,
  `user` int NOT NULL,
  `role` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `channels_roles`
--

INSERT INTO `channels_roles` (`channel`, `user`, `role`) VALUES
(1, 1, 2),
(2, 2, 2),
(3, 1, 2),
(4, 4, 2),
(5, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int NOT NULL,
  `creator` int NOT NULL,
  `content` text NOT NULL,
  `creation_date` datetime NOT NULL,
  `edit_date` datetime DEFAULT NULL,
  `post` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id`, `creator`, `content`, `creation_date`, `edit_date`, `post`) VALUES
(1, 1, 'Conclusion from the paper:\r\n   &quot; A variety of evidence points to what the authors believe can only be plausibly explained by systematic falsification and manipulation of official organ transplant datasets in China. Some apparently nonvoluntary donors also appear to be misclassified as voluntary. This takes place alongside genuine voluntary organ transplant activity, which is often incentivized by large cash payments. These findings are relevant for international interactions with China’s organ transplantation system.&quot;\r\nEvidence in r/dataisbeautiful suggests China is doing this again with the coronavirus outbreak numbers. That seems incredibly irresponsible.', '2020-02-10 16:56:50', '2020-02-10 16:56:50', 2),
(2, 2, 'Very interesting article thank you', '2020-02-10 18:05:43', '2020-02-10 18:05:43', 1),
(3, 1, 'Glad you liked it !', '2020-02-10 18:07:35', '2020-02-10 18:07:35', 1),
(4, 4, 'AHAHAHAH', '2020-02-10 18:16:25', '2020-02-10 18:16:25', 7),
(5, 4, 'I like waffles.', '2020-02-10 18:17:01', '2020-02-10 18:17:01', 6);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int NOT NULL,
  `from` int NOT NULL,
  `to` int NOT NULL,
  `content` text NOT NULL,
  `send_date` date NOT NULL,
  `delivery_date` date DEFAULT NULL,
  `read_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `id` int NOT NULL,
  `creator` int NOT NULL,
  `title` varchar(256) NOT NULL,
  `content` text NOT NULL,
  `creation_date` datetime NOT NULL,
  `edit_date` datetime DEFAULT NULL,
  `channel` int NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `creator`, `title`, `content`, `creation_date`, `edit_date`, `channel`, `published`) VALUES
(1, 1, 'Farmed shrimp  worse for the environment than rainforest.', 'A very interessant article to read, we can debate it in the comments below.\r\n\r\n[The link to the article](https://journalistcorner.news/2020/02/10/prawn-predicaments/)', '2020-02-10 16:53:30', '2020-02-10 16:53:30', 1, 1),
(2, 1, 'China probably faked its organ transplant data', '[Link to the article](https://bmcmedethics.biomedcentral.com/articles/10.1186/s12910-019-0406-6#citeas)', '2020-02-10 16:55:47', '2020-02-10 16:55:47', 1, 1),
(3, 1, 'New \'reaper of death\' tyrannosaur is the oldest found in Canada', '**There is tyrannosaur in the title it seems like a fine article.**\r\n\r\n[TYRANNOSAURS RULES](https://www.cbc.ca/news/technology/new-tyrannosaur-thatan-1.3236678)', '2020-02-10 17:01:52', '2020-02-10 17:01:52', 1, 1),
(4, 2, 'Researchers virtually unwind lithium battery for the first time', 'Don\'t hesitate to comment under this post, I would love to know what you think about this new discovery. Thanks guys !\r\n\r\n[the link to the article](https://www.eurekalert.org/pub_releases/2020-02/tfi-rv020720.php)', '2020-02-10 17:04:40', '2020-02-10 17:04:40', 1, 1),
(5, 2, 'She is a beauty', 'Look at her blep !\r\n\r\n![](https://pbs.twimg.com/profile_images/888907252702347265/g2JwwLDR_400x400.jpg)', '2020-02-10 17:21:08', '2020-02-10 17:21:08', 2, 1),
(6, 1, 'Rupert is always stealing my food', 'What a dork \r\n![](https://pbs.twimg.com/profile_images/1092090332546777088/heg4PZd8_400x400.jpg)', '2020-02-10 17:23:35', '2020-02-10 17:23:35', 2, 1),
(7, 1, 'Ok, now what ?', '[](https://preview.redd.it/4vo4szjqxzf41.jpg?width=640&amp;crop=smart&amp;auto=webp&amp;s=5aab23b55dd6a92d067040148a72c4a1a9308fed)', '2020-02-10 17:57:04', '2020-02-10 17:57:04', 5, 1),
(8, 1, 'And here. We. Go!', '![](https://preview.redd.it/i6bbhlalrxf41.jpg?width=960&amp;crop=smart&amp;auto=webp&amp;s=0f93d6578120130388183a2d9987299c5399c7ef)', '2020-02-10 18:09:29', '2020-02-10 18:09:29', 4, 1),
(9, 4, 'Discovery of virus with no recognizable genes found in animals', '[Link to the article](https://www.sciencemag.org/news/2020/02/scientists-discover-virus-no-recognizable-genes?utm_campaign=news_daily_2020-02-07&amp;amp;amp;amp;et_rid=486754869&amp;amp;amp;amp;et_cid=3198410)', '2020-02-10 18:14:07', '2020-02-10 18:14:07', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `reactions_comment`
--

CREATE TABLE `reactions_comment` (
  `comment` int NOT NULL,
  `user` int NOT NULL,
  `value` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reactions_post`
--

CREATE TABLE `reactions_post` (
  `post` int NOT NULL,
  `user` int NOT NULL,
  `value` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `reactions_post`
--

INSERT INTO `reactions_post` (`post`, `user`, `value`) VALUES
(1, 1, 1),
(1, 2, 1),
(3, 4, 1),
(4, 4, 1),
(6, 4, 1),
(7, 4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int NOT NULL,
  `name` varchar(64) NOT NULL,
  `default` tinyint(1) DEFAULT '0',
  `global_admin` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `default`, `global_admin`) VALUES
(1, 'Super admin', 0, 1),
(2, 'Channel admin', 0, 0),
(3, 'Moderator', 0, 0),
(4, 'User', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `roles_permissions`
--

CREATE TABLE `roles_permissions` (
  `role` int NOT NULL,
  `permission` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `roles_permissions`
--

INSERT INTO `roles_permissions` (`role`, `permission`) VALUES
(2, 'channel-edit'),
(2, 'channel-manage'),
(2, 'channel-role'),
(2, 'comment-delete'),
(2, 'post-delete'),
(2, 'post-edit'),
(2, 'post-publish'),
(3, 'channel-manage'),
(3, 'comment-delete'),
(3, 'post-delete'),
(3, 'post-edit'),
(3, 'post-publish');

-- --------------------------------------------------------

--
-- Structure de la table `subscribers`
--

CREATE TABLE `subscribers` (
  `user` int NOT NULL,
  `channel` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `email` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `bio` text NOT NULL,
  `profile_picture` varchar(2048) NOT NULL,
  `reg_date` date NOT NULL,
  `global_admin` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `password`, `bio`, `profile_picture`, `reg_date`, `global_admin`) VALUES
(1, 'laura.gangloff@orange.fr', 'laura_gangloff', '$2y$10$iREcv57UqKI89OgjzZpkXeSLMMb1QQTRhFiEpsQH3ULGk9bJDJzVa', '', 'http://sharead.rousseur.fr:5555/assets/media/images/avatar.png', '2020-02-10', 0),
(2, 'yann.hodiesne@gmail.com', 'yann_hodiesne', '$2y$10$F0fBV.yV2EbK1ymYsaaFXeQmou7CzcPs/fDuecx.OMQeqrIJXOq.y', 'Just a guy who likes science, dogs, and funny meme.', 'http://sharead.rousseur.fr:5555/uploads/2e251e969bbf0f00b17c9e2b497bc6cf.jpg', '2020-02-10', 0),
(3, 'admin@admin.admin', 'admin', '$2y$10$PF5/JjO0P8MIbZC6TkpZA.8/mZjpcIeU5q3JbbK4ZXR7yyI1hDEw6', '', 'http://sharead.rousseur.fr:5555/assets/media/images/avatar.png', '2020-02-10', 1),
(4, 'moog@cesi.fr', 'Moogli', '$2y$10$PRPFiGl2Fp/jqF2Yb2JdaODgGEi1AvEXkJZB6H8V02SLJvrKLw/re', '', 'http://sharead.rousseur.fr:5555/assets/media/images/avatar.png', '2020-02-10', 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `channels_roles`
--
ALTER TABLE `channels_roles`
  ADD KEY `fk_channels_roles_channel` (`channel`),
  ADD KEY `fk_channels_roles_role` (`role`),
  ADD KEY `fk_channels_roles_user` (`user`);

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_comment` (`creator`),
  ADD KEY `fk_post_comment` (`post`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_messages_from_user` (`from`),
  ADD KEY `fk_messages_to_user` (`to`);

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_channel_post` (`channel`),
  ADD KEY `fk_user_post` (`creator`);

--
-- Index pour la table `reactions_comment`
--
ALTER TABLE `reactions_comment`
  ADD PRIMARY KEY (`user`,`comment`),
  ADD KEY `fk_comment_reactions_comment` (`comment`);

--
-- Index pour la table `reactions_post`
--
ALTER TABLE `reactions_post`
  ADD PRIMARY KEY (`post`,`user`),
  ADD KEY `fk_reactions_post_user` (`user`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD PRIMARY KEY (`role`,`permission`);

--
-- Index pour la table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`user`,`channel`),
  ADD KEY `fk_subscriber_user` (`user`),
  ADD KEY `fk_subscriber_channel` (`channel`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `channels`
--
ALTER TABLE `channels`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `channels_roles`
--
ALTER TABLE `channels_roles`
  ADD CONSTRAINT `fk_channels_roles_channel` FOREIGN KEY (`channel`) REFERENCES `channels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_channels_roles_role` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_channels_roles_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_post_comment` FOREIGN KEY (`post`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_comment` FOREIGN KEY (`creator`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `fk_messages_from_user` FOREIGN KEY (`from`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_messages_to_user` FOREIGN KEY (`to`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `fk_channel_post` FOREIGN KEY (`channel`) REFERENCES `channels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_post` FOREIGN KEY (`creator`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `reactions_comment`
--
ALTER TABLE `reactions_comment`
  ADD CONSTRAINT `fk_comment_reactions_comment` FOREIGN KEY (`comment`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_reactions_comment` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reactions_post`
--
ALTER TABLE `reactions_post`
  ADD CONSTRAINT `fk_reactions_post_post` FOREIGN KEY (`post`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reactions_post_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD CONSTRAINT `fk_roles_permissions_role` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `subscribers`
--
ALTER TABLE `subscribers`
  ADD CONSTRAINT `fk_subscriber_channel` FOREIGN KEY (`channel`) REFERENCES `channels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_subscriber_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
