SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sharead`
--

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE IF NOT EXISTS `channels` (
  `id` int NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `banner` varchar(2048) NOT NULL,
  `author` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

-- --------------------------------------------------------

--
-- Table structure for table `channels_roles`
--

CREATE TABLE IF NOT EXISTS `channels_roles` (
  `channel` int NOT NULL,
  `user` int NOT NULL,
  `role` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int NOT NULL,
  `creator` int NOT NULL,
  `content` text NOT NULL,
  `creation_date` datetime NOT NULL,
  `edit_date` datetime,
  `post` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int NOT NULL,
  `from` int NOT NULL,
  `to` int NOT NULL,
  `content` text NOT NULL,
  `send_date` date NOT NULL,
  `delivery_date` date,
  `read_date` date
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int NOT NULL,
  `creator` int NOT NULL,
  `title` varchar(256) NOT NULL,
  `content` text NOT NULL,
  `creation_date` datetime NOT NULL,
  `edit_date` datetime,
  `channel` int NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

-- --------------------------------------------------------

--
-- Table structure for table `reactions_comment`
--

CREATE TABLE IF NOT EXISTS `reactions_comment` (
  `comment` int NOT NULL,
  `user` int NOT NULL,
  `value` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

-- --------------------------------------------------------

--
-- Table structure for table `reactions_post`
--

CREATE TABLE IF NOT EXISTS `reactions_post` (
  `post` int NOT NULL,
  `user` int NOT NULL,
  `value` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int NOT NULL,
  `name` varchar(64) NOT NULL,
  `default` tinyint(1) DEFAULT 0,
  `global_admin` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

-- --------------------------------------------------------

--
-- Table structure for table `roles_permissions`
--

CREATE TABLE IF NOT EXISTS `roles_permissions` (
  `role` int NOT NULL,
  `permission` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
  `user` int NOT NULL,
  `channel` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL,
  `email` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  `bio` text NOT NULL,
  `profile_picture` varchar(2048) NOT NULL,
  `reg_date` date NOT NULL,
  `global_admin` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_comment` (`creator`),
  ADD KEY `fk_post_comment` (`post`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_messages_from_user` (`from`),
  ADD KEY `fk_messages_to_user` (`to`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_channel_post` (`channel`),
  ADD KEY `fk_user_post` (`creator`);

--
-- Indexes for table `reactions_comment`
--
ALTER TABLE `reactions_comment`
  ADD PRIMARY KEY (`user`,`comment`),
  ADD KEY `fk_comment_reactions_comment` (`comment`);

--
-- Indexes for table `reactions_post`
--
ALTER TABLE `reactions_post`
  ADD PRIMARY KEY (`post`,`user`),
  ADD KEY `fk_reactions_post_user` (`user`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD PRIMARY KEY (`role`, `permission`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`user`,`channel`),
  ADD KEY `fk_subscriber_user` (`user`),
  ADD KEY `fk_subscriber_channel` (`channel`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `channels`
--
ALTER TABLE `channels`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `channels_roles`
--
ALTER TABLE `channels_roles`
  ADD CONSTRAINT `fk_channels_roles_channel` FOREIGN KEY (`channel`) REFERENCES `channels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_channels_roles_role` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_channels_roles_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_post_comment` FOREIGN KEY (`post`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_comment` FOREIGN KEY (`creator`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `fk_messages_from_user` FOREIGN KEY (`from`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_messages_to_user` FOREIGN KEY (`to`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `fk_channel_post` FOREIGN KEY (`channel`) REFERENCES `channels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_post` FOREIGN KEY (`creator`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `reactions_comment`
--
ALTER TABLE `reactions_comment`
  ADD CONSTRAINT `fk_comment_reactions_comment` FOREIGN KEY (`comment`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_reactions_comment` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reactions_post`
--
ALTER TABLE `reactions_post`
  ADD CONSTRAINT `fk_reactions_post_post` FOREIGN KEY (`post`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reactions_post_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `roles_permissions`
--
ALTER TABLE `roles_permissions`
  ADD CONSTRAINT `fk_roles_permissions_role` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD CONSTRAINT `fk_subscriber_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_subscriber_channel` FOREIGN KEY (`channel`) REFERENCES `channels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Values for table `roles`
--
INSERT INTO `roles` (`id`, `name`, `default`, `global_admin`) VALUES
  (1, 'Super admin', 0, 1),
  (2, 'Channel admin', 0, 0),
  (3, 'Moderator', 0, 0),
  (4, 'User', 1, 0);

--
-- Values for table `roles_permissions`
--
INSERT INTO `roles_permissions` (`role`, `permission`) VALUES
  (2, 'channel-edit'),
  (2, 'post-edit'),
  (2, 'post-delete'),
  (2, 'post-publish'),
  (2, 'comment-delete'),
  (2, 'channel-manage'),
  (2, 'channel-role'),
  (3, 'post-edit'),
  (3, 'channel-manage'),
  (3, 'post-delete'),
  (3, 'post-publish'),
  (3, 'comment-delete');

COMMIT;
