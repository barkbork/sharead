<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller {
	/**
     * Controller for /users/
     */
	public function index() {
		redirect('users/profile');
	}
	
	/**
     * /users/:id : displays given user profile, or the current user profile if :id is not specified
     */
	public function profile(int $id = NULL) {
		if ($id === NULL && !is_logged_in()) {
            set_toast('You must log in to continue');
			redirect('users/signin');
		}
		
		$this->load->model('post');
		$this->load->model('comment');
		$this->load->model('channel');

		$this->load->model('managers/user_manager');
		$this->load->model('managers/post_manager');
		$this->load->model('managers/comment_manager');
		$this->load->model('managers/channel_manager');

		$user = new User($this->user_manager->get($id ?? get_account_id()));
		$channel_manager = $this->channel_manager;

		$is_own_account = true;

		if($id !== null) {
			$is_own_account = false;
		}

		// If the user could not be found in database
		if ($user->getId() === NULL) {
			// If an id has been provided, it means the given user does not exist
			if ($id !== NULL) {
                set_toast('This user does not exist');
				redirect('/users/profile');
			} else {
				// If there was no id provided, the session is corrupt and needs to be reset
				redirect('/users/signout');
			}
		}

		$posts_count = $this->user_manager->get_posts_count($user->getId());
		$reputation = $this->user_manager->get_reputation($user->getId());
		$p_history = $this->post_manager->get_post_history($user->getId());
		$c_history = $this->comment_manager->get_comment_history($user->getId());
		
		$channels = array();
		$list_post = array();
		$list_comment = array();

		foreach ($p_history as $posts_history) {
			$post = new Post($posts_history);
			array_push($list_post, $post);
			array_push($channels, new Channel($channel_manager->get_by_id($post->getChannel())));
		}

		foreach($c_history as $comments_history) {
			array_push($list_comment, new Comment($comments_history));
		}
		
		$data['header'] = $this->load->view('header', NULL, TRUE);

		$data['content'] = $this->load->view('profile', array(
			'user' => $user,
			'posts_count' => $posts_count,
			'reputation' => $reputation,
			'post_history' => $list_post,
			'comment_history' => $list_comment,
			'enable_edit' => $is_own_account,
			'channels' => $channels
		), TRUE);
		
		$data['footer'] = $this->load->view('footer', NULL, TRUE);

		$this->load->view('main', $data);
    }
	
	/**
	 * /users/signin : let the user sign in to his account
	 */
    public function signin() {
		if (is_logged_in()) {
			redirect('/users/profile');
		}
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('user');
		$this->load->model('managers/user_manager');

		$this->form_validation->set_rules('login', 'Login', 'trim|required|htmlspecialchars');
		$this->form_validation->set_rules('password', 'Password', 'required|htmlspecialchars');

		// Checks if input is valid
		if ($this->form_validation->run()) {
			// Checks if we can log the user in
			$user = new User($this->user_manager->get_by_login($this->input->post('login')));

			if ($user->getId() !== NULL && password_verify($this->input->post('password'), $user->getPassword())) {
				// Store the user in current session
				$this->session->user_id = $user->getId();
				$this->session->user_name = $user->getName();

                set_toast('Welcome back!');
				redirect('/');
			} else {
				$this->form_validation->set_rules('null', 'null', 'required', array(
					'required' => 'This account does not exist or given password does not match'
				));
				$this->form_validation->run();
			}
		}

		// Show the form, with potential errors
		$data['header'] = $this->load->view('header', NULL, TRUE);
        $data['content'] = $this->load->view('login', NULL, TRUE);
		$data['footer'] = $this->load->view('footer', NULL, TRUE);
		
		$this->load->view('main', $data);
	}

	/**
	 * /users/signup : let the user create a new account 
	 */
    public function signup() {
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('recaptcha');
		$this->load->model('user');
		$this->load->model('managers/user_manager');

		$this->form_validation->set_rules('username', 'Name', 'trim|required|alpha_dash|min_length[5]|max_length[64]|is_unique[users.name]|htmlspecialchars', array(
			'alpha_dash' => 'The username cannot contain special characters',
			'min_length' => 'This username is too short',
			'max_length' => 'This username is too long',
			'is_unique' => 'This username is already taken'
		));
		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[64]|is_unique[users.email]|htmlspecialchars', array(
			'valid_email' => 'The provided email address is not valid',
			'max_length' => 'This email address is too long',
			'is_unique' => 'This email address is already in use'
		));
		
		$this->form_validation->set_rules('password', 'Password', 'required|max_length[50]|htmlspecialchars', array(
			'max_length' => 'This password is too long'
		));
		
		$this->form_validation->set_rules('confirmPwd', 'Password confirmation', 'required|matches[password]|htmlspecialchars', array(
			'matches' => 'Given passwords did not match'
		));

		$this->form_validation->set_rules('g-recaptcha-response', 'reCaptcha', 'required', array(
			'required' => 'Please complete the anti-robot check'
		));

		// Checks if input is valid
		if ($this->form_validation->run()) {
			// Checks if reCaptcha validated the request
			$recaptcha = $this->input->post('g-recaptcha-response');
			$response = $this->recaptcha->verifyResponse($recaptcha);

			if (($response['success'] ?? false) === true) {
				// User creation
				$user = new User(array(
					'name' => $this->input->post('username'),
					'email' => $this->input->post('email'),
					'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
					'bio' => '',
					'profile_picture' => base_url('/assets/media/images/avatar.png'),
					'reg_date' => date('Y-m-d H:i:s')
				));

				$this->user_manager->insert($user);

                set_toast('Welcome, newcomer! Please log in to ensure you remember your password');
				redirect('users/signin');
			}
		}

		// Show the form, with potential errors
		$data['header'] = $this->load->view('header', NULL, TRUE);

		$data['content'] = $this->load->view('signup', array(
			'captcha' => $this->recaptcha->getWidget(array('data-theme' => 'dark'))
		), TRUE);
		
		$data['footer'] = $this->load->view('footer', NULL, TRUE);
		$data['scripts'] = array($this->recaptcha->getScriptTag());
		
		$this->load->view('main', $data);
	}

	/**
	 * /users/signout : disconnect the user
	 */
	public function signout() {
		$this->session->unset_userdata(array('user_id', 'user_name'));
        set_toast('You are now disconnected');
		redirect('/users/signin');
	}

	/**
	 * /users/update : let the user update his profile informations, password and avatar
	 */
	 public function update() {
         $this->load->helper('form');
         $this->load->library('form_validation');
         $this->load->model('user');
         $this->load->model('managers/user_manager');

         $this->form_validation->set_rules('bio', 'Bio', 'trim|max_length[64]|htmlspecialchars', array(
            'min_length' => 'This bio is too short',
            'max_length' => 'This bio is too long',
         ));

         $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[64]|htmlspecialchars', array(
             'valid_email' => 'The provided email address is not valid',
             'max_length' => 'This email address is too long'
         ));

         $this->form_validation->set_rules('password', 'Password', 'max_length[50]|htmlspecialchars', array(
             'max_length' => 'This password is too long'
         ));

         $this->form_validation->set_rules('password_repeat', 'Password confirmation', 'matches[password]|htmlspecialchars', array(
             'matches' => 'Given passwords did not match'
         ));

         // Checks if input is valid
		 if ($this->form_validation->run()) {
		     $config['upload_path'] = './uploads';
             $config['allowed_types'] = 'gif|jpg|jpeg|png';
             $config['max_size'] = '20480';
             $config['encrypt_name'] = TRUE;

             $this->load->library('upload', $config);

             // User update
             $user = new User(array(
                 'id' => get_account_id(),
                 'bio' => $this->input->post('bio'),
                 'email' => $this->input->post('email')
             ));

			 // If a profile pic has been given, update it
             if ($this->upload->do_upload('profilePic')) {
                 $user->setProfilePicture(base_url('/uploads/' . $this->upload->data()['file_name']));
             }

			 // If a new password has been given, update it
             if (!empty($this->input->post('password'))) {
                 $user->setPassword(password_hash($this->input->post('password'), PASSWORD_BCRYPT));
             }

             $this->user_manager->update(get_account_id(), $user);

             set_toast('Your profile has been updated');
             redirect('users/profile');
		 }

		 $user = new User($this->user_manager->get(get_account_id()));

		 $data['header'] = $this->load->view('header', NULL, TRUE);

		 $data['content'] = $this->load->view('update_user', array(
		     'bio' => $user->getBio(),
             'email' => $user->getEmail()
         ), TRUE);
		
		 $data['footer'] = $this->load->view('footer', NULL, TRUE);

		 $this->load->view('main', $data);
	 }
}
