<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Search extends CI_Controller
{
    /**
     * Search controller
     * /search/
     * Let the user search for channels, posts and other users
     */
    public function index()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->load->model('user');
        $this->load->model('managers/user_manager');
        $this->load->model('channel');
        $this->load->model('managers/channel_manager');
        $this->load->model('post');
        $this->load->model('managers/post_manager');

        $this->form_validation->set_rules('query', 'Query', 'trim|required|htmlspecialchars');

        $users = array();
        $channels = array();
        $posts = array();

        // Checks if input is valid
        if ($this->form_validation->run()) {
            $query = $this->input->post('query');

            foreach($this->user_manager->search($query) as $user) {
                array_push($users, new User($user));
            }

            foreach($this->channel_manager->search($query) as $channel) {
                array_push($channels, new Channel($channel));
            }

            foreach($this->post_manager->search($query) as $post) {
                array_push($posts, new Post($post));
            }
        }

        // Show the form, with potential errors
        $data['header'] = $this->load->view('header', NULL, TRUE);

        $data['content'] = $this->load->view('search', array(
            'users' => $users,
            'channels' => $channels,
            'posts' => $posts
        ), TRUE);

        $data['footer'] = $this->load->view('footer', NULL, TRUE);

        $this->load->view('main', $data);
    }
}
