<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Comments extends CI_Controller
{
	public function index()
	{
        redirect('');
    }

	/**
	 * /comments/create/:post_id : Creates given comment in given post
	 */
    public function create(int $id = null) {
		if (!is_logged_in()) {
            set_toast('You must log in to continue');
			redirect('users/signin');
		}

		if ($id === null) {
			redirect('');
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('comment');
		$this->load->model('managers/comment_manager');

		$this->form_validation->set_rules('content', 'Content', 'trim|required|max_length[2048]|htmlspecialchars', array(
			'max_length' => 'Comments must not exceed 2048 chars',
		));

		// Checks if input is valid
		if ($this->form_validation->run()) {
			// Comment creation
			$comment = new Comment(array(
				'creator'  => get_account_id(),
				'content' => $this->input->post('content'),
				'creation_date' => date('Y-m-d H:i:s'),
				'edit_date' => date('Y-m-d H:i:s'),
				'post' => $id
			));
			
			$this->comment_manager->insert($comment);
			
		}

		redirect('posts/see/' . $id);
	}

    /**
	 * /comments/delete/:comment_id : Delete given comment, if the current user is its author or has the permission to do so
	 */
    public function delete(int $id = null) {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        if ($id === null) {
            redirect('');
        }

        $this->load->model('comment');
        $this->load->model('managers/comment_manager');
        $this->load->model('post');
        $this->load->model('managers/post_manager');

        $comment = new Comment($this->comment_manager->get($id));
        $post = new Post($this->post_manager->get($comment->getPost()));

        if (is_null($comment)) {
            redirect('');
        }

        if (intval($comment->getCreator()) === intval(get_account_id()) || has_permission($post->getChannel(), get_account_id(), 'comment-delete')) {
            $this->comment_manager->delete($comment->getId());
        }

        redirect('/posts/see/' . $comment->getPost());
    }

    /**
	 * /comments/upvote : Upvote given comment (+1)
	 */
    public function upvote() {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        $id = @$this->input->post('id');

        if (is_numeric($id)) {
            $id = intval($id);

            echo $this->react(get_account_id(), $id, 1);
        } else {
            http_response_code(400);
            echo 'Bad request';
        }
    }

    /**
	 * /comments/downvote : Downvote given comment (-1)
	 */
    public function downvote()
    {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        $id = @$this->input->post('id');

        if (is_numeric($id)) {
            $id = intval($id);

            echo $this->react(get_account_id(), $id, -1);
        } else {
            http_response_code(400);
            echo 'Bad request';
        }
    }

    /**
	 * Not a route
     * Is used by upvote and downvote to handle database interactions to update the user's reaction
	 */
    private function react($user_id, $comment_id, $value) {
        $this->load->model('managers/comment_manager');

        $reaction = $this->comment_manager->get_reaction($comment_id, $user_id);

        if (is_null($reaction)) {
            $this->comment_manager->add_reaction($comment_id, $user_id, $value);
            return "INSERT";
        } else if (intval($reaction->value) === intval($value)) {
            $this->comment_manager->delete_reaction($comment_id, $user_id);
            return "DELETE";
        } else {
            $this->comment_manager->update_reaction($comment_id, $user_id, $value);
            return "UPDATE";
        }
    }
}
