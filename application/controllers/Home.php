<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	/**
	 * /home/ : Home of the website, with our logo, trending channels list and trending posts list
	 */
	public function index()
	{
	    $this->load->library('markdown');
	    $this->load->helper('text');

		$this->load->model('channel');
		$this->load->model('managers/channel_manager');
		$this->load->model('post');
		$this->load->model('managers/post_manager');
		$this->load->model('user');
		$this->load->model('managers/user_manager');
		$this->load->helper('date');

		$last_channel_list = array();
		$last_post_list = array();
		
		$channels = $this->channel_manager->get_channels(3, 0);
		$posts = $this->post_manager->get_posts(4,0);
		$user_manager = $this->user_manager;

		$scores = array();
		$comments = array();
		$countStyle = 1;
		$users = array();
		$channel_post = array();

		foreach($channels as $channel) {
			array_push($last_channel_list, new Channel($channel));
		}

		foreach($posts as $value) {
		    $post = new Post($value);
		    $post->setContent(ellipsize($this->markdown->parse($post->getContent()), 256));
			array_push($last_post_list, new Post($post));
			array_push($users, new User($user_manager->get_by_id($post->getCreator())));
			array_push($channel_post, new Channel($this->channel_manager->get_by_id($post->getChannel())));
		}

		foreach($last_post_list as $key) {
			array_push($scores, $this->post_manager->get_score($key->getId()) ?? 0);
			array_push($comments, $this->post_manager->get_comment_count($key->getId()) ?? 0);
		}

		
		$data['header'] = $this->load->view('header', NULL, TRUE);
		$data['banner'] = $this->load->view('banner', NULL, TRUE);

		$data['content'] = $this->load->view('home', array(
			'channels' => $last_channel_list,
			'posts' => $last_post_list,
			'countStyle' => $countStyle,
			'scores' => $scores,
			'comments' => $comments,
			'users' => $users,
			'channel_post' => $channel_post,
		), TRUE);

		$data['footer'] = $this->load->view('footer', NULL, TRUE);
		$this->load->view('main', $data);
	}
}
