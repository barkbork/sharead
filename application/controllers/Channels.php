<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Channels extends CI_Controller
{
	/**
	 * Controller for /channels/
	 */
	public function index()
	{
		redirect('/channels/seeAll');
	}

	/**
	 * /channels/seeAll : displays a list of all available channels
	 */
	public function seeAll() {
		$this->load->model('channel');
		$this->load->model('managers/channel_manager');
		$this->load->library("pagination");
		$this->load->helper('url');

		//Configuration load from custom pagination helper
		$this->config->load('pagination', TRUE);

		//Get count of all the channels in the database
		$nb_channels = $this->channel_manager->get_count();

		//Declaration of settings used to configure the paginator
		$settings = $this->config->item('pagination');

		//Declaration of the number of results shown per page
		$settings['per_page'] = 4;

		//Declaration of the url segment used for the pagination's redirection	
		$settings["uri_segment"] = 3;

		//Declaration of the total number of rows found for channel
		$settings["total_rows"] = $nb_channels;

		//Base url used by the pagination for the redirection
		$settings['base_url'] = site_url('/channels/seeAll');

		//Test on the uri's segment
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$list_channel = array();
		
		if($nb_channels > 0) {
			//Pagination is now initialized with all of our settings
			$this->pagination->initialize($settings);
			
			//Get channels with a limit of 4 in the request and the current index
			$channelsAll = $this->channel_manager->get_channels($settings["per_page"], $page);
			
			foreach ($channelsAll as $channel) {
				array_push($list_channel, new Channel($channel));
			}
		}

		$data['header'] = $this->load->view('header', NULL, TRUE);

		$data['content'] = $this->load->view('see_all_channels',  array(
		    'title' => 'Discover all the wonderous channels',
			'channels' => $list_channel,
			'links' => $this->pagination->create_links(),
		), TRUE);

		$data['footer'] = $this->load->view('footer', NULL, TRUE);
		
		$this->load->view('main', $data);
	}

	/**
	 * /channels/mine : displays a list of all channels where the current user has a role (admin or moderator)
	 */
    public function mine() {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        $this->load->model('channel');
        $this->load->model('managers/channel_manager');
        $this->load->library("pagination");
        $this->load->helper('url');

        //Configuration load from custom pagination helper
        $this->config->load('pagination', TRUE);

        //Get count of all the channels in the database
        $nb_channels = $this->channel_manager->get_my_channels_count(get_account_id());

        //Declaration of settings used to configure the paginator
        $settings = $this->config->item('pagination');

        //Declaration of the number of results shown per page
        $settings['per_page'] = 4;

        //Declaration of the url segment used for the pagination's redirection
        $settings["uri_segment"] = 3;

        //Declaration of the total number of rows found for channel
        $settings["total_rows"] = $nb_channels;

        //Base url used by the pagination for the redirection
        $settings['base_url'] = site_url('/channels/mine');

        //Test on the uri's segment
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $list_channel = array();

        if($nb_channels > 0) {
            //Pagination is now initialized with all of our settings
            $this->pagination->initialize($settings);

            //Get channels with a limit of 4 in the request and the current index
            $channelsAll = $this->channel_manager->get_my_channels(get_account_id(), $settings["per_page"], $page);

            foreach ($channelsAll as $channel) {
                array_push($list_channel, new Channel($channel));
            }
        }

        $data['header'] = $this->load->view('header', NULL, TRUE);

        $data['content'] = $this->load->view('see_all_channels',  array(
            'title' => 'My channels',
            'channels' => $list_channel,
            'links' => $this->pagination->create_links(),
        ), TRUE);

        $data['footer'] = $this->load->view('footer', NULL, TRUE);

        $this->load->view('main', $data);
    }

	/**
	 * /channels/see/:name/:sort : displays a list of published posts in given channel, with the ability to sort by scores or date
	 */
	public function see(string $name = '', string $sortBy = 'hot')
	{
		if ($name === '') {
			redirect('channels/seeAll');
		}

		$this->load->library('markdown');
		$this->load->helper('text');

		$this->load->model('channel');
		$this->load->model('managers/channel_manager');
		$this->load->model('post');
		$this->load->model('managers/post_manager');
		$this->load->model('comment');
		$this->load->library("pagination");
		$this->load->helper('url');
		$this->load->helper('date');
		$this->load->model('user');
		$this->load->model('managers/user_manager');

		$channel = new Channel($this->channel_manager->get_by_name($name));

		// If the channel does not exist yet, redirect to creation
        if ($channel->getId() === NULL) {
            set_toast('This channel does not exist, do you want to be its owner?');
            redirect('channels/create/' . $name);
        }

        $pending_count = 0;

		// If the user can manage this channel
        if (is_logged_in() && has_permission($channel, get_account_id(), 'channel-manage')) {
            $pending_count = $this->post_manager->get_pending_posts_count($channel->getId());
        }

		$user_manager = $this->user_manager;
		
		//Configuration loaded from custom pagination helper
		$this->config->load('pagination', TRUE);

		//Get count of all the post in the channel in the database
		$nb_posts_in_channel = $this->post_manager->count_posts_in_channel($channel->getId());

		//Declaration of settings used to configure the paginator
		$settings = $this->config->item('pagination');

		//Declaration of the number of results shown per page
		$settings['per_page'] = 4;
		
		//Declaration of the url segment used for the pagination's redirection	
		$settings["uri_segment"] = 4;

		//Declaration of the total number of rows found for the posts in the channel
		$settings["total_rows"] = $nb_posts_in_channel;
	
		$settings['base_url'] = base_url() . "index.php/channels/see/" . $name;

		//Test on the uri's segment
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

		$posts_data = array();
		
		//Get posts in channel with a limit of 4 and the current index
		if($sortBy === 'hot') {
			$posts_data = $this->post_manager->get_top_posts_from_channel($channel->getId(), $settings["per_page"], $page);
		}
		else {
			$posts_data = $this->post_manager->get_posts_from_channel($channel->getId(), $settings["per_page"], $page);
		}

		$posts = array();
		$score = array();
		$comments = array();
		$users = array();

		foreach ($posts_data as $value) {
		    $post = new Post($value);
		    $post->setContent(ellipsize($this->markdown->parse($post->getContent()), 256));
			array_push($posts, $post);
			array_push($users, new User($user_manager->get_by_id($post->getCreator())));
		}

		if($nb_posts_in_channel > 0) {
			//Pagination is now initialized with all of our settings
			$this->pagination->initialize($settings);
		}

		foreach ($posts as $key) {
			array_push($score, $this->post_manager->get_score($key->getId()) ?? 0);
			array_push($comments, $this->post_manager->get_comment_count($key->getId()) ?? 0);
		}

		$data['header'] = $this->load->view('header', NULL, TRUE);

		$data['content'] = $this->load->view('see_channel', array(
			'channel' => $channel,
			'posts' => $posts,
			'score' => $score,
			'comments' => $comments,
			'links' => $this->pagination->create_links(),
			'users' => $users,
            'pending_count' => $pending_count
		), TRUE);

		$data['footer'] = $this->load->view('footer', NULL, TRUE);
		
		$this->load->view('main', $data);
	}

	/**
	 * Channel creation view
	 * Creates the channel if received form data is valid
	 */
    public function create(string $name = '') {
		if (!is_logged_in()) {
		    set_toast('You must log in to continue');
			redirect('users/signin');
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('channel');
		$this->load->model('managers/channel_manager');

		$this->form_validation->set_rules('name', 'Name', 'trim|required|alpha_dash|min_length[2]|max_length[64]|is_unique[channels.name]|htmlspecialchars', array(
			'alpha_dash' => 'The channel name cannot contain special characters',
			'min_length' => 'This channel name is too short',
			'max_length' => 'This channel name is too long',
			'is_unique' => 'This channel name is already taken'
		));
		
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[1024]|htmlspecialchars', array(
			'max_length' => 'A short description of the channel is required',
		));

		$upload_error = '';

		// Checks if input is valid
		if ($this->form_validation->run()) {
            $config['upload_path'] = './uploads';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size'] = '20480';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('banner')) {
                $upload_error = $this->upload->display_errors('<div class="error">', '</div>');
            } else {
                // Channel creation
                $channel = new Channel(array(
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'banner' => base_url('/uploads/' . $this->upload->data()['file_name']),
                    'author' => get_account_id()
                ));

                $channel_id = $this->channel_manager->insert($channel);

				// Give the user admin access to his new channel
                add_role($channel_id, get_account_id(), CHANNEL_ADMIN);

                set_toast('Welcome to your new channel!');
                redirect('channels/see/' . $channel->getName());
            }
		}

		// Show the form, with potential errors
		$data['header'] = $this->load->view('header', NULL, TRUE);

		$data['content'] = $this->load->view('create_channel', array(
			'default_name' => $name,
            'upload_error' => $upload_error
		), TRUE);

		$data['footer'] = $this->load->view('footer', NULL, TRUE);
		
		$this->load->view('main', $data);
	}

    /**
     * Channel edition view
     * Edits the channel if received form data is valid and the current user has the permission to do so
     */
    public function edit(string $name = '') {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        $this->load->model('channel');
        $this->load->model('managers/channel_manager');

        $channel = new Channel($this->channel_manager->get_by_name($name));

        if (is_null($channel->getId())) {
            redirect('');
        }

		// If the current user cannot edit the channel, redirects to /channels/see
        if (!has_permission($channel, get_account_id(), 'channel-edit')) {
            redirect('channels/see/' . $channel->getName());
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim|required|alpha_dash|min_length[2]|max_length[64]|htmlspecialchars', array(
            'alpha_dash' => 'The channel name cannot contain special characters',
            'min_length' => 'This channel name is too short',
            'max_length' => 'This channel name is too long',
            'is_unique' => 'This channel name is already taken'
        ));

        $this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[1024]|htmlspecialchars', array(
            'max_length' => 'A short description of the channel is required',
        ));

        // Checks if input is valid
        if ($this->form_validation->run()) {
            $config['upload_path'] = './uploads';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size'] = '20480';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            // Channel update
            $channel->setName($this->input->post('name'));
            $channel->setDescription($this->input->post('description'));

			// If the user specified a new banner, update it
            if ($this->upload->do_upload('banner')) {
                $channel->setBanner(base_url('/uploads/' . $this->upload->data()['file_name']));
            }

            $this->channel_manager->update($channel->getId(), $channel);

            set_toast('Channel edited successfully!');
            redirect('channels/see/' . $channel->getName());
        }

        // Show the form, with potential errors
        $data['header'] = $this->load->view('header', NULL, TRUE);

        $data['content'] = $this->load->view('edit_channel', array(
            'channel' => $channel
        ), TRUE);

        $data['footer'] = $this->load->view('footer', NULL, TRUE);

        $this->load->view('main', $data);
    }

	/**
	 * /channels/manage/:name : Channel management page, to allow posts to be published and manage your channel's roles
	 */
	public function manage(string $name = '') {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        if ($name === '') {
			redirect('channels/seeAll/');
		}

		$this->load->helper('inflector');

		$this->load->model('user');
		$this->load->model('managers/user_manager');
		$this->load->model('channel');
		$this->load->model('managers/channel_manager');
		$this->load->model('post');
		$this->load->model('managers/post_manager');
		$this->load->model('role');
		$this->load->model('managers/role_manager');
		$this->load->model('managers/permission_manager');

		$channel = new Channel($this->channel_manager->get_by_name($name));

		if (is_null($channel->getId())) {
		    redirect('/channels/seeAll');
        }

		if (!has_permission($channel, get_account_id(), 'channel-manage')) {
		    redirect('/channels/see/' . $name);
        }

		$creator = new User($this->user_manager->get($channel->getAuthor()));

		$recent_posts = array();

		foreach ($this->post_manager->get_posts_from_channel($channel->getId(), 5) as $post) {
            array_push($recent_posts, array(
                'post' => new Post($post),
                'creator' => new User($this->user_manager->get($post['creator']))
            ));
        }

		$pending_posts = array();

		foreach ($this->post_manager->get_pending_posts($channel->getId(), 5) as $post) {
		    array_push($pending_posts, array(
		        'post' => new Post($post),
                'creator' => new User($this->user_manager->get($post['creator']))
            ));
        }

		$roles = array();

		foreach ($this->role_manager->get_standard_roles() as $role) {
		    $role = new Role($role);
		    $permissions = array();
            $users = array();

		    foreach ($this->permission_manager->get_permissions_for_role($role->getId()) as $permission) {
                array_push($permissions, $permission['permission']);
            }

            foreach ($this->role_manager->get_users_with_role($channel->getId(), $role->getId()) as $user) {
                array_push($users, new User($user));
		    }

		    array_push($roles, array(
		        'role' => $role,
                'permissions' => $permissions,
                'users' => $users
            ));
        }

		$data['header'] = $this->load->view('header', NULL, TRUE);

		$data['content'] = $this->load->view('channel_administration', array(
			'channel' => $channel,
			'creator' => $creator,
			'recent_posts' => $recent_posts,
			'pending_posts' => $pending_posts,
            'roles' => $roles
		), TRUE);

		$data['footer'] = $this->load->view('footer', NULL, TRUE);
		$data['scripts'] = array('<script src="' . base_url('/assets/js/custom.js') . '"></script>');
		
		$this->load->view('main', $data);
	}

	/**
	 * /channels/add_role/:name : Add given user to given role inside the channel, if the current user has the permission to do so
	 */
	public function add_role(string $channel = '') {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        $this->load->model('channel');
        $this->load->model('managers/channel_manager');
        $this->load->model('user');
        $this->load->model('managers/user_manager');
        $this->load->model('role');
        $this->load->model('managers/role_manager');

        $channel = new Channel($this->channel_manager->get_by_name($channel));
        $user = new User($this->user_manager->get_by_login($this->input->post('user')));
        $role = new Role($this->role_manager->get($this->input->post('role')));

        if (!is_null($channel->getId()) && !is_null($user->getId()) && !is_null($role->getId()) && has_permission($channel, get_account_id(), 'channel-role')) {
            add_role($channel, $user, $role);
        } else {
			set_toast('Could not add role to given user');
		}

        redirect('/channels/manage/' . $channel->getName() . "#roles");
    }

	/**
	 * /channels/remove_role : Remove given user from its role in given channel, if the current user has the right to do so
	 */
	public function remove_role(string $channel = '') {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

		$this->load->model('channel');
        $this->load->model('managers/channel_manager');
        $this->load->model('user');
        $this->load->model('managers/user_manager');

		$channel = new Channel($this->channel_manager->get_by_name($channel));
		$user = new User($this->user_manager->get($this->input->post('user')));

		if (is_null($channel->getId()) || is_null($user->getId()) || !has_permission($channel, get_account_id(), 'channel-role')) {
		    http_response_code(400);
		    die();
        }

        delete_role($channel, $user);
	}
}
