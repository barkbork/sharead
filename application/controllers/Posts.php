<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Posts extends CI_Controller
{
    /**
     * Controller for /posts/
     */
	public function index()
	{
		redirect('/channels/seeAll');
    }
    
    /**
     * /posts/see : displays given post if current user has the permission
     */
	public function see(int $id = null) {
		if ($id === null)
			redirect('');

		$this->load->library('markdown');

		$this->load->model('post');
		$this->load->model('managers/post_manager');
		$this->load->model('comment');
		$this->load->model('managers/comment_manager');
		$this->load->model('user');
		$this->load->model('managers/user_manager');
		$this->load->model('comment');
		$this->load->model('channel');
		$this->load->model('managers/channel_manager');
		$this->load->helper('date');

		$post_manager = $this->post_manager;
		$user_manager = $this->user_manager;
		$comment_manager = $this->comment_manager;
		$channel_manager = $this->channel_manager;

		$post = new Post($post_manager->get_by_id($id));
		$channel = new Channel($channel_manager->get_by_id($post->getChannel()));

        // If the post is not yet published, check if the current user can open it before continuing
        if ($post->getPublished() == FALSE) {
            if (!is_logged_in() || !has_permission($post->getChannel(), get_account_id(), 'post-publish')) {
                redirect('/channels/see/' . $channel->getName());
            }
        }

        $creator = new User($user_manager->get_by_id($post->getCreator()));

        $post->setContent($this->markdown->parse($post->getContent()));

		$array = $post_manager->get_comments($id);
		$comments = array();
		$users = array();
		$upvotes_comment = array();
		$downvotes_comment = array();

		foreach ($array as $key) {
			$comment = new Comment($key);
			array_push($comments, $comment);
			array_push($users, new User($this->user_manager->get_by_id($comment->getCreator())));
			array_push($upvotes_comment, $comment_manager->get_upvotes_count($comment->getId()));
			array_push($downvotes_comment, $comment_manager->get_downvotes_count($comment->getId()));
		}

        $data['header'] = $this->load->view('header', NULL, TRUE);

        $data['content'] = $this->load->view('see_post', array(
			'post' => $post,
			'comment_post_count' => $post_manager->get_comment_count($id),
			'upvotes_post_count' => $post_manager->get_upvotes_count($id),
			'downvotes_post_count' => $post_manager->get_downvotes_count($id),
			'comments' => $comments,
			'users' => $users,
			'upvotes_comment' => $upvotes_comment,
			'downvotes_comment' => $downvotes_comment,
			'channel' => $channel,
			'creator' => $creator
        ), TRUE);

        if (is_logged_in()) {
            $data['scripts'] = array(
                '<script src="' . base_url('/assets/js/custom.js') . '"></script>'
            );
        }

        $data['footer'] = $this->load->view('footer', NULL, TRUE);

		$this->load->view('main', $data);
	}

    /**
     * /posts/mine : displays a list of current user's posts
     */
	public function mine() {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        $this->load->model('post');
        $this->load->model('managers/post_manager');
        $this->load->model('channel');
        $this->load->model('managers/channel_manager');
        $this->load->library("pagination");
        $this->load->library("markdown");
        $this->load->helper('text');
        $this->load->helper('date');

        //Configuration load from custom pagination helper
        $this->config->load('pagination', TRUE);

        //Get count of all the posts in the database
        $nb_posts = $this->post_manager->get_post_history_count(get_account_id());

        //Declaration of settings used to configure the paginator
        $settings = $this->config->item('pagination');

        //Declaration of the number of results shown per page
        $settings['per_page'] = 6;

        //Declaration of the url segment used for the pagination's redirection
        $settings["uri_segment"] = 3;

        //Declaration of the total number of rows found for channel
        $settings["total_rows"] = $nb_posts;

        //Base url used by the pagination for the redirection
        $settings['base_url'] = site_url('/posts/mine');

        //Test on the uri's segment
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $posts = array();
        $channels = array();
        $scores = array();
        $comments = array();

        if($nb_posts > 0) {
            //Pagination is now initialized with all of our settings
            $this->pagination->initialize($settings);

            //Get posts with a limit of 6 in the request and the current index
            $posts_data = $this->post_manager->get_post_history(get_account_id(), $settings["per_page"], $page);

            foreach ($posts_data as $post) {
                $post = new Post($post);
                $post->setContent(ellipsize($this->markdown->parse($post->getContent()), 256));
                array_push($posts, $post);
                array_push($channels, new Channel($this->channel_manager->get($post->getChannel())));
                array_push($scores, $this->post_manager->get_score($post->getId()));
                array_push($comments, $this->post_manager->get_comment_count($post->getId()));
            }
        }

        $data['header'] = $this->load->view('header', NULL, TRUE);

        $data['content'] = $this->load->view('see_my_posts',  array(
            'posts' => $posts,
            'channels' => $channels,
            'scores' => $scores,
            'comments' => $comments,
            'links' => $this->pagination->create_links(),
        ), TRUE);

        $data['footer'] = $this->load->view('footer', NULL, TRUE);

        $this->load->view('main', $data);
    }

	/**
     * /posts/create/:channel_id : let the current user create a post in given channel
     */
    public function create(int $id = null) {
		if (!is_logged_in()) {
            set_toast('You must log in to continue');
			redirect('users/signin');
		}

		if ($id === null) {
			redirect('');
		}

		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('post');
		$this->load->model('managers/post_manager');

		$this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[64]|htmlspecialchars', array(
			'min_length' => 'This post name is too short',
			'max_length' => 'This post name is too long',
		));

		$this->form_validation->set_rules('content', 'Content', 'trim|required|max_length[2048]|htmlspecialchars', array(
			'max_length' => 'The post must not exceed 2048 chars',
		));
		
		// Checks if input is valid
		if ($this->form_validation->run()) {
			// Post creation
			$post = new Post(array(
				'creator'  => get_account_id(),
				'title' => $this->input->post('title'),
				'content' => $this->input->post('content'),
				'creation_date' => date('Y-m-d H:i:s'),
				'edit_date' => date('Y-m-d H:i:s'),
				'channel' => $id,
                'published' => FALSE
			));
			
			$id = $this->post_manager->insert($post);

            set_toast('Your post has been sent to the channel\'s moderators and is waiting for their approval');
			redirect('posts/see/' . $id);
		}

		// Show the form, with potential errors
		$data['header'] = $this->load->view('header', NULL, TRUE);

		$data['content'] = $this->load->view('create_post', NULL, TRUE);

		$data['scripts'] = array(
			'<script src="' . base_url('/assets/js/simplemde.min.js') . '"></script>',
			'<script> var simplemde = new SimpleMDE({ element: $("#content")[0] }); </script>'
		);

		$data['footer'] = $this->load->view('footer', NULL, TRUE);
		
		$this->load->view('main', $data);
	}

    /**
     * /posts/edit/:post_id : let the current user edit the given post if he is its author or has the permission to do so
     */
    public function edit(int $id = null) {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        if ($id === null) {
            redirect('');
        }

        $this->load->model('post');
        $this->load->model('managers/post_manager');

        $post = new Post($this->post_manager->get_by_id($id));

        if (is_null($post)) {
            redirect('');
        }

        if (!has_permission($post->getChannel(), get_account_id(), 'post-edit') && intval($post->getCreator()) !== intval(get_account_id())) {
            redirect('posts/see/' . $post->getId());
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[64]|htmlspecialchars', array(
            'min_length' => 'This post name is too short',
            'max_length' => 'This post name is too long',
        ));

        $this->form_validation->set_rules('content', 'Content', 'trim|required|max_length[2048]|htmlspecialchars', array(
            'max_length' => 'The post must not exceed 2048 chars',
        ));

        // Checks if input is valid
        if ($this->form_validation->run()) {
            // Post update
            $post->setTitle($this->input->post('title'));
            $post->setContent($this->input->post('content'));
            $post->setEditDate(date('Y-m-d H:i:s'));
            $post->setPublished(FALSE);

            $this->post_manager->update($post->getId(), $post);

            set_toast('Your post has been successfully edited, and needs to be approved by the moderators');
            redirect('posts/see/' . $post->getId());
        }

        // Show the form, with potential errors
        $data['header'] = $this->load->view('header', NULL, TRUE);

        $data['content'] = $this->load->view('edit_post', array(
            'post' => $post
        ), TRUE);

        $data['scripts'] = array(
            '<script src="' . base_url('/assets/js/simplemde.min.js') . '"></script>',
            '<script> var simplemde = new SimpleMDE({ element: $("#content")[0] }); </script>'
        );

        $data['footer'] = $this->load->view('footer', NULL, TRUE);

        $this->load->view('main', $data);
    }

    /**
     * /posts/allow/:id : Set published state of given post to VISIBLE if the current user has the permission to do so
     */
    public function allow(int $id = null) {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        if ($id === null) {
            redirect('');
        }

        $this->load->model('post');
        $this->load->model('managers/post_manager');

        $post = new Post($this->post_manager->get_by_id($id));

        if (is_null($post)) {
            redirect('');
        }

        // If the post is already published, redirect to its preview
        if ($post->getPublished() == TRUE) {
            redirect('/posts/see/' . $post->getId());
        }

        // If the current user has the permission to publish a post
        if (has_permission($post->getChannel(), get_account_id(), 'post-publish')) {
            $post->setPublished(TRUE);
            $this->post_manager->update($post->getId(), $post);

            set_toast('This post is now public!');
            redirect('/posts/see/' . $post->getId());
        } else {
            redirect('');
        }
    }

    /**
     * /posts/deny/:id : Deletes unpublished post if the current user has the permission to do so
     */
    public function deny(int $id = null) {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        if ($id === null) {
            redirect('');
        }

        $this->load->model('post');
        $this->load->model('managers/post_manager');
        $this->load->model('channel');
        $this->load->model('managers/channel_manager');

        $post = new Post($this->post_manager->get_by_id($id));
        $channel = new Channel($this->channel_manager->get($post->getChannel()));

        if (is_null($post)) {
            redirect('');
        }

        // If the post is already published, redirect to its preview
        if ($post->getPublished() == TRUE) {
            redirect('/posts/see/' . $post->getId());
        }

        // If the current user has the permission to deny a post
        if (has_permission($post->getChannel(), get_account_id(), 'post-publish')) {
            $this->post_manager->delete($post->getId());

            set_toast('Post successfully deleted');
            redirect('/channels/see/' . $channel->getName());
        } else {
            redirect('');
        }
    }

    /**
     * /posts/delete/:id : Deletes an already published post
     */
    public function delete(int $id = null) {
        if (!is_logged_in()) {
            redirect('users/signin');
        }

        if ($id === null) {
            redirect('');
        }

        $this->load->model('post');
        $this->load->model('managers/post_manager');
        $this->load->model('channel');
        $this->load->model('managers/channel_manager');

        $post = new Post($this->post_manager->get_by_id($id));
        $channel = new Channel($this->channel_manager->get($post->getChannel()));

        if (is_null($post)) {
            redirect('');
        }

        // If the current user is the post's author or has the permission to delete posts
        if (intval($post->getCreator()) === intval(get_account_id()) || has_permission($post->getChannel(), get_account_id(), 'post-delete')) {
            $this->post_manager->delete($post->getId());
        }

        redirect('/channels/see/' . $channel->getName());
    }

    /**
	 * /posts/upvote : Upvote given post (+1)
	 */
	public function upvote() {
		if (!is_logged_in()) {
            set_toast('You must log in to continue');
			redirect('users/signin');
		}

		$id = @$this->input->post('id');

		if (is_numeric($id)) {
		    $id = intval($id);

		    echo $this->react(get_account_id(), $id, 1);
        } else {
		    http_response_code(400);
		    echo 'Bad request';
        }
	}

    /**
	 * /posts/upvote : Downvote given post (-1)
	 */
    public function downvote()
    {
        if (!is_logged_in()) {
            set_toast('You must log in to continue');
            redirect('users/signin');
        }

        $id = @$this->input->post('id');

        if (is_numeric($id)) {
            $id = intval($id);

            echo $this->react(get_account_id(), $id, -1);
        } else {
            http_response_code(400);
            echo 'Bad request';
        }
    }

    /**
	 * Not a route
     * Is used by upvote and downvote to handle database interactions to update the user's reaction
	 */
	private function react($user_id, $post_id, $value) {
        $this->load->model('managers/post_manager');

        $reaction = $this->post_manager->get_reaction($post_id, $user_id);

        if (is_null($reaction)) {
            $this->post_manager->add_reaction($post_id, $user_id, $value);
            return "INSERT";
        } else if (intval($reaction->value) === intval($value)) {
            $this->post_manager->delete_reaction($post_id, $user_id);
            return "DELETE";
        } else {
            $this->post_manager->update_reaction($post_id, $user_id, $value);
            return "UPDATE";
        }
    }
}
