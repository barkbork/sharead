<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Returns a humanized time difference between now and given timestamp
function setStringDateCreation($creation_date) {
    $timestamp = strtotime($creation_date);

    $strTime = array("second", "minute", "hour", "day", "month", "year");
    $length = array("60","60","24","30","12","10");

    $currentTime = time();

    if ($currentTime >= $timestamp) {
        $diff = time() - $timestamp;

        for ($i = 0; $diff >= $length[$i] && $i < count($length) - 1; $i++) {
            $diff = $diff / $length[$i];
        }

        $diff = round($diff);
        return $diff . " " . $strTime[$i] . ($diff != 1 ? "s" : "") ." ago ";
    } else {
        return "";
    }
}
