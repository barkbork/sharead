<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Returns TRUE if given user has the given permission in specified channel, otherwise returns FALSE
 */
function has_permission($channel, $user, $permission) {
    /**
     * @var CI_Controller $controller
     */
    $controller = &get_instance();
    $controller->load->model('channel');
    $controller->load->model('role');
    $controller->load->model('managers/role_manager');
    $controller->load->model('managers/permission_manager');

    // Retrieve given user from the database
    if (!($user instanceof User)) {
        $controller->load->model('managers/user_manager');

        $user = new User($controller->user_manager->get($user));
    }

    // If the user is a global administrator, give him all the permissions
    if ($user->getGlobalAdmin()) {
        return true;
    }

    // Retrieve given channel from the database
    if (!($channel instanceof Channel)) {
        $controller->load->model('managers/channel_manager');

        $channel = new Channel($controller->channel_manager->get($channel));
    }

    $array_role = $controller->role_manager->get_role($channel->getId(), $user->getId());

    // If the user does not have a role in this channel, takes the default role
    if ($array_role !== NULL) {
        $user_role = $array_role->role;
    } else {
        $user_role = $controller->role_manager->get_default_role()->id;
    }

    $role = new Role($controller->role_manager->get($user_role));

    return $controller->permission_manager->get_permission($role->getId(), $permission) !== NULL;
}

/**
 * Set given role for given user in specifies channel
 */
function add_role($channel, $user, $role) {
    /**
     * @var CI_Controller $controller
     */
    $controller = &get_instance();
    $controller->load->model('channel');
    $controller->load->model('role');
    $controller->load->model('managers/role_manager');
    $controller->load->model('managers/permission_manager');

    // Retrieve given user from the database
    if (!($user instanceof User)) {
        $controller->load->model('managers/user_manager');

        $user = new User($controller->user_manager->get($user));
    }

    // Retrieve given channel from the database
    if (!($channel instanceof Channel)) {
        $controller->load->model('managers/channel_manager');

        $channel = new Channel($controller->channel_manager->get($channel));
    }

    // Retrieve given role from the database
    if (!($role instanceof Role)) {
        $role = new Role($controller->role_manager->get($role));
    }

    $current_role = $controller->role_manager->get_role($channel->getId(), $user->getId());

    // If the user already has a role in this channel
    if (is_object($current_role)) {
        // If the user is not the channel's author, to prevent having a channel without a channel administrator
        if ($user->getId() !== $channel->getAuthor()) {
            // Delete the current role
            delete_role($channel, $user);
        } else {
            return FALSE;
        }
    }

    return $controller->role_manager->add_role($channel->getId(), $user->getId(), $role->getId());
}

/**
 * Delete given user's current role in specified channel
 */
function delete_role($channel, $user) {
    /**
     * @var CI_Controller $controller
     */
    $controller = &get_instance();
    $controller->load->model('channel');
    $controller->load->model('managers/role_manager');
    $controller->load->model('managers/permission_manager');

    // Retrieve given user from the database
    if (!($user instanceof User)) {
        $controller->load->model('managers/user_manager');

        $user = new User($controller->user_manager->get($user));
    }

    // Retrieve given channel from the database
    if (!($channel instanceof Channel)) {
        $controller->load->model('managers/channel_manager');

        $channel = new Channel($controller->channel_manager->get($channel));
    }

    return $controller->role_manager->delete_role($channel->getId(), $user->getId());
}
