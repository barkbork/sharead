<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Returns TRUE if the user is logged in
function is_logged_in() {
    return isset($_SESSION['user_id']) && $_SESSION['user_id'] !== NULL;
}

// Returns the current user id
function get_account_id() {
    return $_SESSION['user_id'];
}

// Returns the current user name
function get_account_name() {
    return $_SESSION['user_name'];
}
