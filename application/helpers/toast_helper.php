<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Set a toast to display to the user in the next page
function set_toast(string $message) {
    $_SESSION['toast'] = $message;
}

// Check if a toast is present
function find_toast() {
    return isset($_SESSION['toast']);
}

// Returns the toast content and empty it
function get_toast() {
    $message = $_SESSION['toast'];
    unset($_SESSION['toast']);
    return $message;
}
