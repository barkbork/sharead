<section id="three" class="wrapper spotlight style3">
    <div class="inner">
        <div class="content">
            <?= validation_errors('<div class="error">', '</div>') ?>
            <h3 class="major">Edit post</h3>
            <?= form_open() ?>
                <div class="row gtr-uniform">
                    <div class="col-12 col-12-xsmall">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" value="<?= set_value('title', $post->getTitle()) ?>" placeholder="Your beautiful and meaningful title" />
                    </div>
                    <div class="col-12 col-12-xsmall">
                        <label for="content">Content</label>
                        <textarea name="content" id="content"><?= set_value('content', $post->getContent()) ?></textarea>
                    </div>
                    <div class="col-12 col-12-xsmall">
                        <input type="submit" class="button primary" value="Save">
                        <span class="float-right">Your post will need the channel's moderators approval before showing up again</span>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</section>