<section id="trending-posts" class="wrapper alt style3">
    <div class="inner">
        <h2 class="major">Trending channels</h2>
        <?php
        if (count($channels) === 0) {
            echo('<div class="empty">Nothing to see here!</div>');
        }
        ?>
    </div>
</section>

<!-- Channels -->
<?php foreach($channels as $channel) {?>
    <section id="trending-channels" class="wrapper <?= $countStyle === 2 ? 'alt' : '' ?> spotlight style<?= $countStyle++ ?>">
        <div class="inner">
            <a href="<?= site_url('channels/see/' . $channel->getName()) ?>" class="image"><img src="<?= $channel->getBanner() === '' ? base_url('/images/pic07.jpg') : $channel->getBanner() ?>" alt="" /></a>
            <div class="content">
                <a href="<?= site_url('channels/see/' . $channel->getName()) ?>" class='nameChannel'> <h2 class="major"><?= $channel->getName() ?></h2></a>
                <p><?= $channel->getDescription()?></p>
                <div class="d-flex flex-row bd-highlight mb-3">
                    <?= anchor('channels/see/' . $channel->getName(), 'Access', 'class="special mr-5"') ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

<!-- Trending posts -->
<section id="trending-posts" class="wrapper alt style1">
    <div class="inner">
        <h2 class="major">Trending posts</h2>
        <section class="features">
        <?php
        if (count($posts) > 0) {
            for ($i = 0; $i < count($posts); $i++) {
                $post = $posts[$i]; ?>
                <article class="post">
                    <div class="info-post">
                        <span>Posted by <?= anchor('/users/profile/' . $users[$i]->getId(), $users[$i]->getName()) . ' ' . setStringDateCreation($post->getCreationDate()) ?> in <?=anchor('/channels/see/' . $channel_post[$i]->getName(), $channel_post[$i]->getName())?> </span>
                    </div>     
                    <h3 class="major"><a
                                href="<?= site_url('posts/see/' . $post->getId()) ?>"><?= $post->getTitle() ?></a></h3>
                    <p><?= $post->getContent() ?></p>
                    <div class="reactions">
                        <p>
                            <i class="fas fa-angle-double-up"></i>
                            <span><?= $scores[$i] ?></span>
                        </p>
                        <p>
                            <i class="fas fa-comment"></i>
                            <span><?= $comments[$i] ?></span>
                        </p>
                    </div>
                </article>
                <?php
            }
        } else {
            echo('<div class="empty">Nothing to see here!</div>');
        }
        ?>
        </section>
        <!-- <ul class="actions">
            <li><a href="#" class="button">Browse more</a></li>
        </ul> -->
    </div>
</section>

<!-- Follow us -->
<section id="follow">
    <h2 class="text-center text-uppercase font-weight-bold mt-5">follow us</h2>
    <div class="inner pt-5">
        <!-- <div class="d-flex justify-content-between mx-5">
            <span class="p-2 bd-highlight icon solid fa-envelope"><a href="#"></a></span>
            <span class="p-2 bd-highlight icon brands fa-twitter"><a href="#"></a></span>
            <span class="p-2 bd-highlight icon brands fa-facebook-f"><a href="#"></a></span>
            <span class="p-2 bd-highlight icon brands fa-instagram"><a href="#"></a></span>
        </div> -->
        <div class="d-flex justify-content-between mx-5">
            <span class="fa-stack">
                <i class="far fa-circle fa-stack-2x"></i>
                <i class="fa fa-envelope fa-stack-1x"></i>
            </span>
            <span class="fa-stack">
                <i class="far fa-circle fa-stack-2x"></i>
                <i class="fa icon brands fa-twitter fa-stack-1x"></i>
            </span>
            <span class="fa-stack">
                <i class="far fa-circle fa-stack-2x"></i>
                <i class="fa icon brands fa-facebook-f fa-stack-1x"></i>
            </span>
            <span class="fa-stack">
                <i class="far fa-circle fa-stack-2x"></i>
                <i class="fa icon brands fa-instagram fa-stack-1x"></i>
            </span>
        </div>
    </div>
</section>