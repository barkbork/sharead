<section id="three" class="wrapper spotlight style3">
    <div class="inner">
        <div class="content">
            <?= validation_errors('<div class="error">', '</div>') ?>
            <?= $upload_error ?>
            <h3 class="major">Create a new channel</h3>
            <?= form_open_multipart() ?>
                <div class="row gtr-uniform">
                    <div class="col-12 col-12-xsmall">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" value="<?= set_value('name', $default_name) ?>" placeholder="Enter a name" />
                    </div>
                    <div class="col-12 col-12-xsmall">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control" rows="3" placeholder="Describe your channel"><?= set_value('description') ?></textarea>
                    </div>
                    <div class="col-12 col-12-xsmall">
                        <label for="banner-input">Banner</label>
                        <input type="file" name="banner" id="banner-input" class="button" />
                    </div>
                    <div class="col-12 col-12-xsmall" id="submit">
                        <input type="submit" class="button primary" value="Submit" />
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</section>