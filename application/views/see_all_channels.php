<section id="trending-posts" class="wrapper alt style1">
    <div class="inner">
        <h2 class="major"><?= $title ?></h2>
        <h1><?= anchor('channels/create/', 'Create your own channel !', 'class="special mr-5"') ?></h1>
        <section class="features">
            <?php
            if (count($channels) > 0) {
                foreach (array_reverse($channels) as $channel) { ?>
                    <article class="post">
                        <a href="<?= site_url('channels/see/' . $channel->getName()) ?>" class="image">
                            <img src="<?= $channel->getBanner() ?>" alt=""/>
                        </a>
                        <h3 class="major">
                            <a href="<?= site_url('channels/see/' . $channel->getName()) ?>"><?= $channel->getName() ?></a>
                        </h3>
                        <p><?= $channel->getDescription() ?></p>
                        <p>
                            <span><?= anchor('channels/see/' . $channel->getName(), '<i class="fas fa-arrow-circle-right"></i><span> Access </span>') ?></span>
                        </p>
                    </article>
                    <?php
                }
            } else {
                echo('<div class="empty">Nothing to see here!</div>');
            }
            ?>

            <div class="inner">
                <div class="paginator-div">
                    <?= $links ?>
                </div>
            </div>
        </section>
    </div>
</section>