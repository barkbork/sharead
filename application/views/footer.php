<section id="footer">
    <div class="inner">
        <ul class="copyright">
            <li>
                <a href="#">Contact us</a> &middot; 
                <a href="#">Privacy and cookies</a> &middot; 
                <a href="#">Terms of use</a> &middot; 
                <a href="#">&copy; Sharead Inc <?= date('Y') ?></a>
            </li>
        </ul>
    </div>
</section>