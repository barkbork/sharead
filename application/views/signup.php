<section id="three" class="wrapper spotlight style3">
    <div class="inner">
        <div class="content">
            <?= validation_errors('<div class="error">', '</div>') ?>
            <h3 class="major">Sign up</h3>
            <?= form_open() ?>
                <div class="row gtr-uniform">
                    <div class="col-6 col-12-xsmall">
                        <label for="username">Name</label>
                        <input type="text" name="username" id="username" value="<?= set_value('username') ?>" placeholder="Enter your name" maxlength="64" required />
                    </div>
                    <div class="col-6 col-12-xsmall">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" value="<?= set_value('email') ?>" placeholder="Enter your email" maxlength="64" required />
                    </div>
                    <div class="col-6 col-12-xsmall">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" value="<?= set_value('password') ?>" placeholder="Password" maxlength="50" required />
                    </div>
                    <div class="col-6 col-12-xsmall">
                        <label for="confirmPwd">Confirm password</label>
                        <input type="password" name="confirmPwd" id="confirmPwd" value="<?= set_value('confirmPwd') ?>" placeholder="Password confirmation" maxlength="50" required />
                    </div>
                    <div class="col-6 col-12-xsmall">
                        <?= $captcha ?>
                    </div>
                    <div class="col-6 col-12-xsmall" id="signinBtn">
                        <input type="submit" class="button primary" id="signup" value="Register" />
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</section>
