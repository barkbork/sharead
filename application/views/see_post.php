<section id="three">
    <div class="inner">
        <div class="content">
            <section class="features">
                <article id="seePost">
                    <div class="info-post">
                        <img src="<?= $channel->getBanner() ?>" alt="Avatar channel" class="avatar-channel">
                        <?= anchor('/channels/see/' . $channel->getName(), $channel->getName(), array('class' => 'channel-name')) ?>
                        <span>  - Posted by <?= anchor('/users/profile/' . $creator->getId(), $creator->getName()) . ' ' . setStringDateCreation($post->getCreationDate()) ?>  </span>     
                    </div>                    
                    <h1 class="major">
                        <?= $post->getTitle() ?>
                    </h1>
                    <div class='post-content-div'>
                        <p>
                            <?= $post->getContent() ?>
                        </p>
                    </div>

                    <div id="reactions">
                        <span id="post-id" style="display:none;"><?= $post->getId() ?></span>
                        <span id="upVote" class="upvote-post">
                            <i class="fas fa-arrow-alt-circle-up pointer-click" id="upvote-post"></i>
                            <strong><?= $upvotes_post_count ?></strong>
                        </span>
                        <span id="downVote" class="downvote-post">
                            <i class="fas fa-arrow-alt-circle-down pointer-click" id="downvote-post"></i>
                            <strong><?= $downvotes_post_count ?></strong>
                        </span>
                        <span class="comments">
                            <i class="fas fa-comment" id="comment-post"></i>
                            <strong><?= $comment_post_count ?></strong>
                        </span>
                        <?php
                        $show_delete_modal = FALSE;

                        if ($post->getPublished() == TRUE) {
                            if (is_logged_in() && (
                                    has_permission($post->getChannel(), get_account_id(), 'post-delete') ||
                                    intval(get_account_id()) == intval($post->getCreator())
                                )) {
                                echo '<button class="edit-post btn-outline-danger" data-toggle="modal" data-target="#delete-modal">Delete</button>';
                                $show_delete_modal = TRUE;
                            }
                            if (is_logged_in() && (
                                    has_permission($post->getChannel(), get_account_id(), 'post-edit') ||
                                    intval(get_account_id()) == intval($post->getCreator())
                                )) {
                                echo anchor('/posts/edit/' . $post->getId(), '<button class="edit-post">Edit</button>');
                            }
                        } else {
                            echo '<button class="edit-post" data-toggle="modal" data-target="#delete-modal">Deny</button>';
                            echo anchor('/posts/allow/' . $post->getId(), '<button class="edit-post primary">Allow</button>');
                            $show_delete_modal = TRUE;
                        }
                        ?>
                    </div>

                    <form method="post" action="<?= site_url('comments/create/' . $post->getId()) ?>">
                        <h4 class="major">Leave a comment !</h4>
                        <textarea name="content" id="content"></textarea>
                        <div id="leaveComment">
                            <button id="send" type="submit">
                                <i class="fas fa-kiwi-bird"></i>
                                <span id="sendComment"><strong>SEND</strong></span>
                            </button>
                        </div>
                    </form>

                    <?php
                    for($i = 0; $i < count($comments); $i++) {
                        $c = $comments[$i];
                        ?>
                        <div class="user-comment">
                        <div class="info-user">
                            <div class='user-profile'>
                                <a href="<?= site_url('users/profile/' . $users[$i]->getId()) ?>"><img src="<?= $users[$i]->getProfilePicture() ?>" alt="Avatar" class="avatar"></a>
                                <?= anchor('/users/profile/' . $users[$i]->getId(), $users[$i]->getName() . " - " . setStringDateCreation($c->getCreationDate()) , array('class' => 'comment-author')) ?>
                            </div>
                            <div class='delete-comment'>
                            <?php
                                if (is_logged_in() && (
                                        has_permission($post->getChannel(), get_account_id(), 'comment-delete') ||
                                        intval(get_account_id()) == intval($users[$i]->getId())
                                    )) {
                                    echo anchor('/comments/delete/' . $comments[$i]->getId(), '<i class="fas fa-eraser"></i> Delete', array('class' => 'comment-author comment-delete'));
                                }
                                ?>
                            </div>
                        </div>
                        <hr class="comment-delimiter" />
                            <?= $c->getContent() ?>
                            <br/>
                            <div class="react-comment">
                                <span class="upvote-comment" id="comment-<?= $c->getId() ?>">
                                    <i class="fas fa-arrow-alt-circle-up pointer-click"></i>
                                    <strong><?= $upvotes_comment[$i] ?></strong>
                                </span>
                                <span class="downvote-comment" id="comment-<?= $c->getId() ?>">
                                    <i class="fas fa-arrow-alt-circle-down pointer-click"></i>
                                    <strong><?= $downvotes_comment[$i] ?></strong>
                                </span>
                            </div>  
                        </div>
                    <?php } ?>
                </article>
            </section>
        </div>
    </div>
</section>

<?php
if ($show_delete_modal) {
    ?>
        <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="delete-modal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Are you sure ?</h5>
                    </div>
                    <div class="modal-body">
                        You are about to delete this post.
                        This action cannot be undone.
                    </div>
                    <div class="modal-footer">
                        <?= anchor(($post->getPublished() == TRUE ? '/posts/delete/' : '/posts/deny/') . $post->getId(), '<button type="button" class="btn-outline-danger">Yes, I am sure</button>') ?>
                        <button type="button" data-dismiss="modal">Wait</button>
                    </div>
                </div>
            </div>
        </div>
    <?php
}
?>
