<nav class="navbar sticky-top navbar-dark">
    <div class="d-flex flex-row bd-highlight">
        <div class="p-2 bd-highlight"><?= anchor('home', '<img id="header-logo" src="' . base_url('/assets/media/images/logo.png') . '" alt="Sharead" />', array('class' => 'nav-link')) ?></div>
        <div class="p-2 bd-highlight"><?= anchor('channels/seeAll', 'Channels', array('class' => 'nav-link')) ?></div>
        <?php
        if (is_logged_in()) {
            ?>
            <div class="p-2 bd-highlight"><?= anchor('channels/mine', 'My channels', array('class' => 'nav-link')) ?></div>
            <div class="p-2 bd-highlight"><?= anchor('posts/mine', 'My posts', array('class' => 'nav-link')) ?></div>
            <?php
        }
        ?>
    </div>

    <div class="d-flex flex-row">
        <?= form_open('/search', array('class' => 'd-flex flex-row bd-highlight')) ?>
            <input type="text" name="query" placeholder="Search" aria-label="Search" value="<?= set_value('query') ?>" />
            <button type="submit" class="button primary small">Search</button>
        <?= form_close() ?>
        <div class="bd-highlight">
            <?= is_logged_in() ? anchor('users/profile', get_account_name(), 'class="nav-link"') : anchor('users/signin', 'Sign in', 'class="nav-link"') ?>
        </div>
        <div class="bd-highlight">
            <?= is_logged_in() ? anchor('users/signout', 'Log out', 'class="nav-link"') : anchor('users/signup', 'Sign up', 'class="nav-link"') ?>
        </div>
    </div>
</nav>