<section id="three">
    <div class="inner">
        <div class="content">
            <section class="features">
                <div class="d-flex justify-content-around flex-wrap">
                    <div class="row col-8">
                        <article class="col-12">
                            <h2 class="major">
                                <?= anchor('/channels/see/' . $channel->getName(), $channel->getName(), array('class' => 'channel-name')) ?>
                                <span> - <?= anchor('/users/profile/' . $creator->getId(), $creator->getName()) ?></span>
                            </h2>
                            <p>
                                <?= $channel->getDescription() ?>
                            </p>
                            <?php
                            if (has_permission($channel, get_account_id(), 'channel-edit')) {
                                echo '<p>';
                                echo anchor('/channels/edit/' . $channel->getName(), '<i class="fas fa-edit"></i> Edit', array('class' => 'no-underline'));
                                echo '</p>';
                            }
                            ?>
                            <img alt="Banner" id="manage-banner" src="<?= $channel->getBanner() ?>" />
                        </article>
                        <div class="col-12 d-flex justify-content-between pl-0">
                            <article>
                                <h3 class="major">Recent activity</h3>
                                <?php
                                if (count($recent_posts) > 0) {
                                    foreach ($recent_posts as $post) {
                                        ?>
                                        <span class="post-list-element">
                                            <i class="fa fa-clone post-icon"></i> <?= anchor('/users/profile/' . $post['creator']->getId(), $post['creator']->getName()) ?>
                                            posted <?= anchor('/posts/see/' . $post['post']->getId(), $post['post']->getTitle()) ?>
                                        </span>
                                        <?php
                                    }
                                } else {
                                    echo '<div class="empty">Nothing to see here!</div>';
                                }
                                ?>
                            </article>
                            <article>
                                <h3 class="major">Items with actions required</h3>
                                <?php
                                if (count($pending_posts) > 0) {
                                    foreach ($pending_posts as $post) {
                                        ?>
                                        <span class="display-block">
                                            <i class="fa fa-clone post-icon"></i> <?= anchor('/users/profile/' . $post['creator']->getId(), $post['creator']->getName()) ?>
                                            created a new post
                                            : <?= anchor('/posts/see/' . $post['post']->getId(), $post['post']->getTitle()) ?>
                                            . Please <?= anchor('/posts/allow/' . $post['post']->getId(), 'allow') ?>
                                            or <?= anchor('/posts/deny/' . $post['post']->getId(), 'deny') ?> this
                                            action.
                                        </span><br>
                                        <?php
                                    }
                                } else {
                                    echo '<div class="empty">No new content to validate!</div>';
                                }
                                ?>
                            </article>
                        </div>
                    </div>
                    <article class="col-3">
                        <h3 class="major">Permissions overview</h3>
                        <?php
                        foreach ($roles as $role) {
                            ?>
                            <h4><?= $role['role']->getName() ?></h4>
                            <ul class="alt">
                                <?php
                                foreach ($role['permissions'] as $permission) {
                                    echo '<li>' . humanize($permission, '-') . '</li>';
                                }
                                ?>
                            </ul>
                        <?php
                        }
                        ?>
                    </article>
                    <article class="col-11" id="roles">
                        <h3 class="major">
                            Manage user roles
                        </h3>
                        <div class="d-flex justify-content-around flex-wrap">
                            <?php
                            foreach ($roles as $role) {
                                ?>
                                <div class="sub-widget">
                                    <h3><?= $role['role']->getName() ?><span id="role-<?= $role['role']->getId() ?>-badge" class="ml-2 badge badge-primary badge-pill"><?= count($role['users']) ?></span></h3>
                                    <?php
                                    if (count($role['users']) > 0) {
                                        ?>
                                        <ul class="alt user-roles">
                                        <?php
                                        foreach ($role['users'] as $user) {
                                            echo '<li id="user-' . $user->getId() . '">';

                                            if (has_permission($channel, get_account_id(), 'channel-role') && intval($channel->getAuthor()) !== intval($user->getId())) {
                                                echo '<i class="mr-2 fas fa-trash remove-user-role" data-channel="' . $channel->getName() . '" data-user="' . $user->getId() . '" data-role="' . $role['role']->getId() . '"></i>';
                                            }

                                            echo $user->getName() . '</li>';
                                        }
                                        ?>
                                        </ul>
                                        <?php
                                    } else {
                                        echo '<p class="empty">No user with this role.</p>';
                                    } ?>

                                    <?php
                                    if (has_permission($channel, get_account_id(), 'channel-role')) {
                                        echo form_open('/channels/add_role/' . $channel->getName());
                                        ?>
                                        <label for="add-user-role-<?= $role['role']->getId() ?>">Add a new user</label>
                                        <input type="text" name="user"
                                               id="add-user-role-<?= $role['role']->getId() ?>"/>
                                        <input type="hidden" name="role" value="<?= $role['role']->getId() ?>"/>
                                        <?php
                                        echo form_close();
                                    }?>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </article>
                </div>
            </section>
        </div>
    </div>
</section>
