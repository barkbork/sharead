<style>
    #channel-banner {
        background-image: url("<?= $channel->getBanner() ?>");
    }
</style>

<div id="channel-banner" class="bg-image"></div>

<section id="banner" class="channel">
    <div class="inner">
        <h2><?= $channel->getName() ?></h2>
        <p><?= $channel->getDescription() ?></p>
    </div>
</section>
<section id="four" class="wrapper alt style1">
    <div class="inner">
        <?php if (is_logged_in()) {
            if (has_permission($channel, get_account_id(), 'channel-manage')) {
                $link = 'Manage channel';

                if ($pending_count > 0) {
                    $link .= '<span> - ' . $pending_count . ' new post(s) to validate</span>';
                }

                echo '<h1>' . anchor('channels/manage/' . $channel->getName(), $link, 'class="special mr-5"') . '</h1>';
            }
            if (has_permission($channel, get_account_id(), 'channel-edit')) {
                echo '<h1>' . anchor('channels/edit/' . $channel->getName(), 'Edit channel', 'class="special mr-5"') . '</h1>';
            }
        } ?>
        <h1><?= anchor('posts/create/' . $channel->getId(), 'Create post', 'class="special mr-5"') ?></h1>
        <h2 class="major">Trending posts in this channel</h2>
        <div class='tri-post'>
            <div>
                <span class="sort-by">Sort by </span>
                <?= anchor('/channels/see/' . $channel->getName() . '/new', '<button class="tri-hot">new <i class="fas fa-hourglass-half"></i></button>', array('class' => 'no-underline'))?>
                <?= anchor('/channels/see/' . $channel->getName(), '<button class="tri-new">hot <i class="fab fa-hotjar"></i></button>', array('class' => 'no-underline') )?>
            </div>
        </div>
        <section class="features">
            <?php
            if (count($posts) > 0) {
                for ($i = 0; $i < count($posts); $i++) {
                    $post = $posts[$i];
                    ?>
                    <article class="post">
                        <div class="info-post">
                        <span>Posted by <?= anchor('/users/profile/' . $users[$i]->getId(), $users[$i]->getName()) . ' ' . setStringDateCreation($post->getCreationDate()) ?>  </span>     
                        </div>     
                        <h3 class="major">
                            <a href="<?= site_url('posts/see/' . $post->getId()) ?>"><?= $post->getTitle() ?></a>
                        </h3>
                        <p><?= $post->getContent() ?></p>
                        <div class="reactions">
                            <p>
                                <i class="fas fa-angle-double-up"></i>
                                <span><?= $score[$i] ?></span>
                            </p>
                            <p>
                                <i class="fas fa-comment"></i>
                                <span><?= $comments[$i] ?></span>
                            </p>
                        </div>
                    </article>
                    <?php
                }
            } else {
                echo('<div class="empty">Nothing to see here!</div>');
            }
            ?>
            
            <div class="inner">
                <div class="paginator-div">
                    <?= $links ?>
                </div>
            </div>
        </section>
    </div>
</section>