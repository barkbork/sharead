<!DOCTYPE HTML>
<!--
	Solid State by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html lang="en">
	<head>
		<title>Sharead</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="shortcut icon" type="image/png" href="<?= base_url('/assets/media/images/favicon.png') ?>" />
		<link rel="stylesheet" href="<?= base_url('/assets/css/bootstrap.min.css') ?>" />
		<link rel="stylesheet" href="<?= base_url('/assets/css/main.css') ?>" />
		<link rel="stylesheet" href="<?= base_url('/assets/css/custom.css') ?>" />
		<link rel="stylesheet" href="<?= base_url('/assets/css/simplemde.min.css') ?>" />
		<noscript><link rel="stylesheet" href="<?= base_url('/assets/css/noscript.css') ?>" /></noscript>
	</head>
	<body class="is-preload">
        <!-- Page Wrapper -->
        <div id="page-wrapper">
            <!-- Header -->
            <?= isset($header) ? $header : '' ?>
            <!-- Banner -->
            <?= isset($banner) ? $banner : '' ?>
            <!-- Wrapper -->
            <section id="wrapper">
                <?= isset($content) ? $content : '' ?>
            </section>
            <!-- Footer -->
            <?= isset($footer) ? $footer : '' ?>
        </div>

        <!-- Scripts -->
        <script src="<?= base_url('/assets/js/jquery.min.js') ?>"></script>
        <script src="<?= base_url('/assets/js/jquery.scrollex.min.js') ?>"></script>
        <script src="<?= base_url('/assets/js/browser.min.js') ?>"></script>
        <script src="<?= base_url('/assets/js/breakpoints.min.js') ?>"></script>
        <script src="<?= base_url('/assets/js/util.js') ?>"></script>
        <script src="<?= base_url('/assets/js/main.js') ?>"></script>
        <script src="<?= base_url('/assets/js/popper.min.js') ?>"></script>
        <script src="<?= base_url('/assets/js/bootstrap.min.js') ?>"></script>
        <?php
            if (isset($scripts)) {
                foreach ($scripts as $script) {
                    echo($script . "\n");
                }
            }
        ?>

        <?php
        if (find_toast()) {
            ?>
            <!-- Toasts -->
            <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false">
                <div class="toast-body">
                    <?= get_toast() ?>
                    <button type="button" data-dismiss="toast" aria-label="Close">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <script>
                $('.toast').toast('show');
            </script>
            <?php
        }
        ?>
	</body>
</html>
