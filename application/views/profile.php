<style>
    .bg-image {
        background-image: url("<?= base_url('/assets/media/images/channel_banner.jpg') ?>");
    }
</style>

<div class="bg-image"></div>

<section id="banner" class="profile">
</section>

<!-- Four -->
<section id="four" class="wrapper alt style1 user-profile">
    <div class="inner">
        <div class="row">
            <div class="col-4 text-center">
                <span class="image"><img src="<?= $user->getProfilePicture() ?>" alt="Avatar" /></span>
                <p>
                    <i class="fas fa-glass-martini-alt"></i><?= $reputation ?> point<?= $reputation === 1 ? '' : 's' ?><br/>
                    <!-- ROLES -->
                </p>
            </div>
            <div class="col-8">
                <h2 class="major"><?= $user->getName() ?></h2>
                <?php if (is_logged_in() && get_account_id() === $user->getId()) { ?>
                    <?= anchor('users/update', '<button id="profile-edit">EDIT</button>') ?>
                <?php } ?>
                <p><i class="far fa-calendar-alt"></i><?= date("d/m/Y", strtotime($user->getRegDate())) ?></p>
                <p><?= strlen($user->getBio()) === 0 ? "<em>Nothing to show, sad!</em>" : $user->getBio() ?></p>

                <div class="text-right">
                    <i class="attributes fas fa-sticky-note"></i><?= $posts_count ?> post<?= $posts_count === 1 ? '' : 's' ?>
                </div>
            </div>
        </div>
    </div>

    <section class="features">
        <article class="post">
            <h3 class="major"><a href="#">Recent comments</a></h3>
            <?php foreach(array_reverse($comment_history) as $c_history) { ?>
                <p><i class="fas fa-arrow-alt-circle-up"></i> <?= $user->getName()?> posted a comment on <strong><?= anchor('posts/see/' . $c_history->getPost(),  $c_history->getPostTitle(), 'class="link-class"') ?> </strong> </p>
                <hr/>
            <?php }?>
        </article>
    </section>

    <section class="features">
        <article class="post">
            <h3 class="major"><a href="#">Recent posts</a></h3>
            <?php  
            if (count($post_history) > 0) {
                for ($i = 0; $i < count($post_history); $i++) { 
                    $post = $post_history[$i];
                ?>
                <p><i class="fas fa-arrow-alt-circle-up"></i> <?= $user->getName()?> created a post <strong><?= anchor('posts/see/' . $post->getId(),  $post->getTitle(), 'class="link-class"') ?></strong> in the channel <strong> <?= anchor('channels/see/' . $channels[$i]->getName(),  $channels[$i]->getName(), 'class="link-class"') ?></strong></p>
                <hr/>
            <?php } }?>
        </article>
    </section>
</section>