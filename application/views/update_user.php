<section id="three" class="wrapper spotlight style3">
    <div class="inner">
        <div class="content">
            <?= validation_errors('<div class="error">', '</div>') ?>
            <h3 class="major">We want to know more about you ! Complete your profile</h3>
            <?= form_open_multipart() ?>
                <div class="row gtr-uniform">
                    <div class="col-12 col-12-xsmall">
                        <label for="bio">Bio</label>
                        <textarea rows="10" name="bio" id="bio" placeholder="Who are you ?"><?= set_value('bio', $bio) ?></textarea>
                    </div>
                    <div class="col-12 col-12-xsmall">
                        <label for="profilePic">Profile picture</label>
                        <input type="file" name="profilePic" id="profilePic" class="button image-input" />
                    </div>

                    <div class="col-12 col-12-xsmall">
                        <label for="email">Email address</label>
                        <input type="text" name="email" id="email" placeholder="Enter your new email address" value="<?= set_value('email', $email) ?>" />
                    </div>

                    <div class="col-12 col-12-xsmall">
                        <label for="password">New password</label>
                        <input type="password" name="password" id="password" placeholder="Enter your new password" />
                    </div>
                    <div class="col-12 col-12-xsmall">
                        <label for="password_repeat">Repeat password</label>
                        <input type="password" name="password_repeat" id="password_repeat" placeholder="Repeat your new password" />
                    </div>

                    <div id="submit" class="col-12 col-12-xsmall">
                        <input type="submit" class="button primary" value="Save" />
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</section>
