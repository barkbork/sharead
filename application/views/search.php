<section id="four" class="wrapper alt style1 user-profile">
    <div class="inner">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="major">Search results</h1>
            </div>
        </div>
    </div>

    <section class="features">
        <article class="post search-container">
            <h3 class="major">Users</h3>
            <?php
            if (count($users) > 0) {
                foreach ($users as $user) {
                    echo '<a class="search-result" href="' . site_url('/users/profile/' . $user->getId()) . '"><img alt="Avatar" src="' . $user->getProfilePicture() . '" class="avatar" /> ' . $user->getName() . '</a>';
                }
            } else {
                echo '<div class="empty">Nothing to see here!</div>';
            }
            ?>

            <h3 class="major">Channels</h3>
            <?php
            if (count($channels) > 0) {
                foreach ($channels as $channel) {
                    echo '<a class="search-result" href="' . site_url('/channels/see/' . $channel->getName()) . '"><img alt="Banner" src="' . $channel->getBanner() . '" class="avatar" /> ' . $channel->getName() . '</a>';
                }
            } else {
                echo '<div class="empty">Nothing to see here!</div>';
            }
            ?>

            <h3 class="major">Posts</h3>
            <?php
            if (count($posts) > 0) {
                foreach ($posts as $post) {
                    echo '<a class="search-result" href="' . site_url('/posts/see/' . $post->getId()) . '"><i class="fa fa-clone avatar post-icon"></i> ' . $post->getTitle() . '</a>';
                }
            } else {
                echo '<div class="empty">Nothing to see here!</div>';
            }
            ?>
        </article>
    </section>
</section>