<section id="three" class="wrapper spotlight style3">
    <div class="inner">
        <div class="content">
            <?= validation_errors('<div class="error">', '</div>') ?>
            <h3 class="major">Log in</h3>
            <?= form_open() ?>
                <div class="row gtr-uniform">
                    <div class="col-6 col-12-xsmall">
                        <label for="login">Name</label>
                        <input type="text" name="login" id="login" value="<?= set_value('login') ?>" placeholder="Name or email" />
                    </div>
                    <div class="col-6 col-12-xsmall">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" value="<?= set_value('password') ?>" placeholder="Password"/>
                    </div>
                    <div class="col-6 col-12-xsmall">
                        <input type="submit" class="button primary" value="Submit" />
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</section>
