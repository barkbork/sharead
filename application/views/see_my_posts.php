<section id="trending-posts" class="wrapper alt style1">
    <div class="inner">
        <h2 class="major">My posts</h2>
        <section class="features">
            <?php
            if (count($posts) > 0) {
                for ($i = 0; $i < count($posts); $i++) {
                    $post = $posts[$i]; ?>
                    <article class="post">
                        <div class="info-post">
                            <span>Posted <?= setStringDateCreation($post->getCreationDate()) ?> in <?= anchor('/channels/see/' . $channels[$i]->getName(), $channels[$i]->getName()) ?> </span>
                        </div>
                        <h3 class="major"><a
                                href="<?= site_url('posts/see/' . $post->getId()) ?>"><?= $post->getTitle() ?></a></h3>
                        <p><?= $post->getContent() ?></p>
                        <div class="reactions">
                            <p>
                                <i class="fas fa-angle-double-up"></i>
                                <span><?= $scores[$i] ?></span>
                            </p>
                            <p>
                                <i class="fas fa-comment"></i>
                                <span><?= $comments[$i] ?></span>
                            </p>
                        </div>
                    </article>
                    <?php
                }
            } else {
                echo('<div class="empty">Nothing to see here!</div>');
            }
            ?>

            <div class="inner">
                <div class="paginator-div">
                    <?= $links ?>
                </div>
            </div>
        </section>
    </div>
</section>