<section id="three" class="wrapper spotlight style3">
    <div class="inner">
        <div class="content">
            <?= validation_errors('<div class="error">', '</div>') ?>
            <h3 class="major">Create your post</h3>
            <?= form_open() ?>
                <div class="row gtr-uniform">
                    <div class="col-12 col-12-xsmall">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" value="<?= set_value('title') ?>" placeholder="Your beautiful and meaningful title" />
                    </div>
                    <div class="col-12 col-12-xsmall">
                        <label for="content">Content</label>
                        <textarea name="content" id="content"><?= set_value('content') ?></textarea>
                    </div>
                    <div class="col-6 col-12-xsmall">
                        <input type="submit" class="button primary" value="Post">
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</section>