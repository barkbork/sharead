<?php
    
    $config['full_tag_open'] = '<div class="paginator">';
    $config['full_tag_close'] = '</div>';    
        
    $config['next_link'] = 'Next';
    $config['next_tag_open'] = '<span class="nextlink">';
    $config['next_tag_close'] = '</span>';
    
    $config['prev_link'] = 'Prev';
    $config['prev_tag_open'] = '<span class="prevlink">';
    $config['prev_tag_close'] = '</span>';
    
    $config['cur_tag_open'] = '<span class="curlink"><strong>';
    $config['cur_tag_close'] = '</strong></span>';
    
    $config['num_tag_open'] = '<span class="numlink">';
    $config['num_tag_close'] = '</span>';


    
?>