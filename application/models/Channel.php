<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Channel extends SR_Model {
    public $_id;
    public $_name;
    public $_description;
    public $_banner;
    public $_author;

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = $name;
    }

    public function getDescription() {
        return $this->_description;
    }

    public function setDescription($description) {
        $this->_description = $description;
    }

    public function getBanner() {
        return $this->_banner;
    }

    public function setBanner($banner) {
        $this->_banner = $banner;
    }

    public function getAuthor() {
        return $this->_author;
    }

    public function setAuthor($author) {
        $this->_author = $author;
    }
}
