<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends SR_Model {
    private $_id;
    private $_name;
    private $_default;
    private $_global_admin;

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = $name;
    }

    public function getDefault() {
        return $this->_default;
    }

    public function setDefault($default) {
        $this->_default = $default;
    }

    public function getGlobalAdmin() {
        return $this->_global_admin;
    }

    public function setGlobalAdmin($global_admin) {
        $this->_global_admin = $global_admin;
    }
}
