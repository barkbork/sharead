<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Channel_manager extends SR_Manager {
    public function __construct() {
        parent::__construct('channels');
    }

    public function get_by_name($name) {
        return $this->db->get_where('channels', array('name' => $name), 1)->row();
    }

    public function get_by_id($id) {
        return $this->db->get_where('channels', array('id' => $id), 1)->row();
    }

    public function get_count() {
        return $this->db->count_all_results('channels');
    }

    public function get_channels($limit, $start) {
        return $this->db->limit($limit, $start)
                        ->order_by('name', 'DESC')
                        ->get('channels')->result_array();
    }

    public function get_my_channels($user_id, $limit, $start) {
        return $this->db->select('c.*')
                        ->limit($limit, $start)
                        ->join('channels_roles r', 'c.id = r.channel')
                        ->where('r.user', $user_id)
                        ->order_by('c.name', 'DESC')
                        ->get('channels c')->result_array();
    }

    public function get_my_channels_count($user_id) {
        return $this->db->select('c.*')
                    ->join('channels_roles r', 'c.id = r.channel')
                    ->where('r.user', $user_id)
                    ->order_by('c.name', 'DESC')
                    ->count_all_results('channels c');
    }

    public function search($query) {
        return $this->db->like('name', $query)
            ->or_like('description', $query)
            ->get('channels')
            ->result_array();
    }
}
