<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment_manager extends SR_Manager {
    public function __construct() {
        parent::__construct('comments');
    }

    public function get_upvotes_count($id) {
        return $this->db->where('comment', $id)
                        ->where('value', 1)
                        ->count_all_results('reactions_comment');
    }

    public function get_downvotes_count($id) {
        return $this->db->where('comment', $id)
                        ->where('value', -1)
                        ->count_all_results('reactions_comment');
    }

    public function get_comment_history($id) {
        return $this->db->limit(10)
                    ->select('c.id, c.creator, c.content, c.creation_date, c.edit_date, c.post, p.title AS post_title')
                    ->join('posts AS p', 'c.post = p.id')
                    ->where('c.creator', $id)
                    ->order_by('c.creation_date', 'DESC')
                    ->get('comments AS c')->result_array();
    }

    public function get_reaction($comment_id, $user_id) {
        return $this->db->from('reactions_comment')
            ->where('user', $user_id)
            ->where('comment', $comment_id)
            ->get()
            ->row();
    }

    public function add_reaction($comment_id, $user_id, $value) {
        return $this->db->insert('reactions_comment', array(
            'user' => $user_id,
            'comment' => $comment_id,
            'value' => $value
        ));
    }

    public function update_reaction($comment_id, $user_id, $value) {
        return $this->db->where('user', $user_id)
                        ->where('comment', $comment_id)
                        ->update('reactions_comment', array(
                            'value' => $value
                        ));
    }

    public function delete_reaction($comment_id, $user_id) {
        return $this->db->where('user', $user_id)
                        ->where('comment', $comment_id)
                        ->delete('reactions_comment');
    }
}
