<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_manager extends SR_Manager {
    public function __construct() {
        parent::__construct('users');
    }

    public function get_by_id($id) {
        return $this->db->where('id', $id)
                        ->get('users', 1)
                        ->row();
    }

    public function get_by_login($login) {
        return $this->db->where('name', $login)
                        ->or_where('email', $login)
                        ->get('users', 1)
                        ->row();
    }

    public function get_posts_count($id) {
        return $this->db->where('creator', $id)
                        ->count_all_results('posts');
    }

    public function get_reputation($id) {
        $rep_comments = $this->db->select_sum('reactions_comment.value')
                             ->from('reactions_comment')
                             ->join('comments', 'reactions_comment.comment = comments.id')
                             ->where('comments.creator', $id)
                             ->get()->row()->value;
        
        $rep_posts = $this->db->select_sum('reactions_post.value')
                              ->from('reactions_post')
                              ->join('posts', 'reactions_post.post = posts.id')
                              ->where('posts.creator', $id)
                              ->get()->row()->value;

        return $rep_comments + $rep_posts;
    }

    public function search($name) {
        return $this->db->like('name', $name)
                        ->get('users')
                        ->result_array();
    }
}
