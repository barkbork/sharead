<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_manager extends SR_Manager {
    public function __construct() {
        parent::__construct('roles');
    }

    public function get_by_id($id) {
        return $this->db->get_where('roles', array(
            'id' => $id
        ), 1)->row();
    }

    public function get_role($channel, $user) {
        return $this->db->get_where('channels_roles', array(
            'channel' => $channel,
            'user' => $user
        ), 1)->row();
    }

    public function add_role($channel, $user, $role) {
        return $this->db->insert('channels_roles', array(
            'channel' => $channel,
            'user' => $user,
            'role' => $role
        ));
    }

    public function delete_role($channel, $user) {
        return $this->db->delete('channels_roles', array(
            'channel' => $channel,
            'user' => $user
        ));
    }

    public function remove_role($channel, $user, $role) {
        return $this->db->delete('channels_roles', array(
            'channel' => $channel,
            'user' => $user,
            'role' => $role
        ));
    }

    public function get_default_role() {
        return $this->db->get_where('roles', array(
            'default' => TRUE
        ), 1)->row();
    }

    public function get_standard_roles() {
        return $this->db->get_where('roles', array(
            'global_admin' => FALSE,
            'default' => FALSE
        ))->result();
    }

    public function get_users_with_role($channel, $role) {
        return $this->db->select('*')
            ->from('users u')
            ->join('channels_roles r', 'u.id = r.user')
            ->where('r.channel', $channel)
            ->where('r.role', $role)
            ->order_by('u.name')
            ->get()->result_array();
    }
}
