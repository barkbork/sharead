<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission_manager extends SR_Manager {
    public function __construct() {
        parent::__construct('roles_permissions');
    }

    public function get_permission($role_id, $permission) {
        return $this->db->get_where('roles_permissions', array(
            'role' => $role_id,
            'permission' => $permission
        ), 1)->row();
    }

    public function add_permission($role_id, $permission) {
        return $this->db->insert('roles_permissions', array(
            'role' => $role_id,
            'permission' => $permission
        ));
    }

    public function delete_permission($role_id, $permission) {
        return $this->db->delete('roles_permissions', array(
           'role' => $role_id,
           'permission' => $permission
        ));
    }

    public function get_permissions_for_role($role_id) {
        return $this->db->get_where('roles_permissions', array(
            'role' => $role_id,
        ))->result_array();
    }
}
