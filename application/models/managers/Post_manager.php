<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_manager extends SR_Manager {
    public function __construct() {
        parent::__construct('posts');
    }

    public function get_posts($limit, $start = 0) {
        return $this->db->limit($limit, $start)
                        ->where('published', TRUE)
                        ->order_by('creation_date', 'DESC')
                        ->get('posts')->result_array();
    }

    public function get_by_id($id) {
        return $this->db->where('id', $id)
                        ->get('posts')->row();
    }

    public function get_posts_from_channel($id, $limit, $start = 0) {
        return $this->db->limit($limit, $start)
                        ->select('p.*')
                        ->from('posts p')
                        ->where('p.channel', $id)
                        ->where('published', TRUE)
                        ->order_by('edit_date', 'DESC')
                        ->get()
                        ->result_array();
    }

    public function count_posts_in_channel($id) {
        return $this->db->select('p.*')
                        ->where('p.channel', $id)
                        ->where('published', TRUE)
                        ->count_all_results('posts p');
    }

    public function get_comment_count($id) {
        return $this->db->where('post', $id)
                        ->count_all_results('comments');
    }

    public function get_upvotes_count($id) {
        return $this->db->where('post', $id)
                        ->where('value', 1)
                        ->count_all_results('reactions_post');
    }

    public function get_downvotes_count($id) {
        return $this->db->where('post', $id)
                        ->where('value', -1)
                        ->count_all_results('reactions_post');
    }

    public function get_comments($id) {
        return $this->db->select('c.*')
                        ->from('comments c')
                        ->where('c.post', $id)
                        ->order_by('(SELECT count(*) FROM reactions_comment WHERE comment = c.id)', 'DESC')
                        ->get()
                        ->result_array();
    }

    public function get_post_history($id, $limit = 5, $start = 0) {
        return $this->db->limit($limit, $start)
                        ->where('creator', $id)
                        ->where('published', TRUE)
                        ->order_by('creation_date', 'DESC')
                        ->get('posts')->result_array();
    }

    public function get_post_history_count($id) {
        return $this->db->where('creator', $id)
                        ->where('published', TRUE)
                        ->order_by('creation_date', 'DESC')
                        ->count_all_results('posts');
    }

    public function get_pending_posts($channel_id, $limit) {
        return $this->db->limit($limit)
                        ->where('channel', $channel_id)
                        ->where('published', FALSE)
                        ->order_by('edit_date', 'DESC')
                        ->get('posts')->result_array();
    }

    public function get_pending_posts_count($channel_id) {
        return $this->db->where('channel', $channel_id)
                        ->where('published', FALSE)
                        ->count_all_results('posts');
    }

    public function get_score($id) {
        return $this->db->select_sum('value')
                        ->from('reactions_post')
                        ->where('post', $id)
                        ->get()
                        ->row()
                        ->value;
    }

    public function get_reaction($post_id, $user_id) {
        return $this->db->from('reactions_post')
                        ->where('user', $user_id)
                        ->where('post', $post_id)
                        ->get()
                        ->row();
    }

    public function add_reaction($post_id, $user_id, $value) {
        return $this->db->insert('reactions_post', array(
             'user' => $user_id,
             'post' => $post_id,
             'value' => $value
         ));
    }

    public function update_reaction($post_id, $user_id, $value) {
        return $this->db->where('user', $user_id)
                 ->where('post', $post_id)
                 ->update('reactions_post', array(
                     'value' => $value
                 ));
    }

    public function delete_reaction($post_id, $user_id) {
        return $this->db->where('user', $user_id)
                 ->where('post', $post_id)
                 ->delete('reactions_post');
    }

    public function search($query) {
        return $this->db->like('title', $query)
                        ->or_like('content', $query)
                        ->order_by('edit_date', 'DESC')
                        ->get('posts')
                        ->result_array();
    }

    public function get_top_posts_from_channel($id, $limit, $start) {
        return $this->db->limit($limit, $start)
            ->select('p.* ')
            ->from('posts p')
            ->where('p.channel', $id)
            ->where('published', TRUE)
            ->order_by('(SELECT sum(r.value) FROM reactions_post r WHERE post = p.id)', 'DESC')
            ->get()
            ->result_array();
    }
}
