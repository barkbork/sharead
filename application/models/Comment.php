<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends SR_Model {
    public $_id;
    public $_creator;
    public $_content;
    public $_creation_date;
    public $_edit_date;
    public $_post;
    public $_post_title;

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function getCreator() {
        return $this->_creator;
    }

    public function setCreator($creator) {
        $this->_creator = $creator;
    }

    public function getContent() {
        return $this->_content;
    }

    public function setContent($content) {
        $this->_content = $content;
    }

    public function getCreationDate() {
        return $this->_creation_date;
    }

    public function setCreationDate($creation_date) {
        $this->_creation_date = $creation_date;
    }

    public function getEditDate() {
        return $this->_edit_date;
    }

    public function setEditDate($edit_date) {
        $this->_edit_date = $edit_date;
    }

    public function getPost() {
        return $this->_post;
    }

    public function setPost($post) {
        $this->_post = $post;
    }

    public function getPostTitle() {
        return $this->_post_title;
    }

    public function setPostTitle($postTitle) {
        $this->_post_title = $postTitle;
    }
}
