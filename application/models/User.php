<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends SR_Model {
    private $_id;
    private $_email;
    private $_name;
    private $_password;
    private $_bio;
    private $_profile_picture;
    private $_reg_date;
    private $_global_admin;

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function getEmail() {
        return $this->_email;
    }

    public function setEmail($email) {
        $this->_email = $email;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = $name;
    }
    
    public function getPassword() {
        return $this->_password;
    }

    public function setPassword($password) {
        $this->_password = $password;
    }

    public function getBio() {
        return $this->_bio;
    }

    public function setBio($bio) {
        $this->_bio = $bio;
    }

    public function getProfilePicture() {
        return $this->_profile_picture;
    }

    public function setProfilePicture($profile_picture) {
        $this->_profile_picture = $profile_picture;
    }

    public function getRegDate() {
        return $this->_reg_date;
    }

    public function setRegDate($reg_date) {
        $this->_reg_date = $reg_date;
    }

    public function getGlobalAdmin() {
        return $this->_global_admin;
    }

    public function setGlobalAdmin($global_admin) {
        $this->_global_admin = $global_admin;
    }
}
