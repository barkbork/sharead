<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends SR_Model {
    public $_id;
    public $_creator;
    public $_title;
    public $_content;
    public $_creation_date;
    public $_edit_date;
    public $_channel;
    public $_published;

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function getCreator() {
        return $this->_creator;
    }

    public function setCreator($creator) {
        $this->_creator = $creator;
    }

    public function getTitle() {
        return $this->_title;
    }

    public function setTitle($title) {
        $this->_title = $title;
    }

    public function getContent() {
        return $this->_content;
    }

    public function setContent($content) {
        $this->_content = $content;
    }

    public function getCreationDate() {
        return $this->_creation_date;
    }

    public function setCreationDate($creation_date) {
        $this->_creation_date = $creation_date;
    }

    public function getEditDate() {
        return $this->_edit_date;
    }

    public function setEditDate($edit_date) {
        $this->_edit_date = $edit_date;
    }

    public function getChannel() {
        return $this->_channel;
    }

    public function setChannel($channel) {
        $this->_channel = $channel;
    }

    public function getPublished() {
        return $this->_published;
    }

    public function setPublished($published) {
        $this->_published = $published;
    }
}
