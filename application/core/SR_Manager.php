<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * SR_Manager : base class for managers with basic functions for interacting with a table
 */
abstract class SR_Manager extends CI_Model {
    private $_table;

    public function __construct($table) {
        parent::__construct();

        $this->_table = $table;
    }
    
    public function get_list($filters = NULL, $limit = NULL) {
        return $this->db->get_where($this->_table, $filters, $limit)->result_array();
    }

    public function get($id) {
        return $this->db->get_where($this->_table, array('id' => $id), 1)->row();
    }

    public function update($id, $object) {
        return $this->db->update($this->_table, $this->to_array($object), array('id' => $id), 1);
    }

    public function insert($object) {
        $this->db->insert($this->_table, $this->to_array($object));
        return $this->db->insert_id();
    }

    public function delete($id) {
        return $this->db->delete($this->_table, array('id' => $id));
    }

    private function to_array($object) {
        $array = array();

        foreach ((array) $object as $key => $value) {
            if ($value !== NULL)
                $array[substr($key, strpos($key, '_') + 1)] = $value;
        }
        
        return $array;
    }
}
