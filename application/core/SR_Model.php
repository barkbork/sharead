<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * SR_Model : base class for models with basic functions for hydratation
 */
abstract class SR_Model {
    public function __construct($array = NULL) {
        if ($array !== NULL) {
            $this->hydrate($array);
        }
    }

    protected function hydrate($array) {
        foreach ($array as $key => $value) {
            $setter = 'set' . ucfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $key))));
            
            if (method_exists($this, $setter)) {
                $this->$setter($value);
            }
        }
    }
}
